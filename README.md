# Effigy

##General
Effigy is an application for testing the accuracy of image search algorithms. 
It includes a frontend in which users can browse a set of images and view pictures deemed similar by the aforementioned algorithms. 
They can assess that similarity by voting (on the scale from 1 to 4). 


The application ecosystem includes a backend server, analyser, mobile app and elasticsearch plugin.
The core part of our system is a server where the application itself is deployed.
It runs both Apache Tomcat server with our application on it and analysis box which
is a standalone module. 



## Storage
Initially we decided to use cloud-based approach due to its 
flexibility in configuration and common access from everywhere. First of all, the user of our application has to create a Google 
cloud storage bucket and upload desired images to it.

For testing purposes we provide two buckets created by us: one of them is empty with write permission to anybody but it is available on demand by asking permission from the authors, the second one is populated with the set of 250 marked images. 
The Second bucket’s name is `images_testing`. Images which should be used as `parent` should be prefixed with `parent` string.
    
**Note:** If no image is prefixed with `parent` the application will work but no results will be displayed.

## Running application

* Prior running application make sure that [Elasticsearch](https://www.elastic.co/) and [MongoDB](https://www.mongodb.com/) instances are running
* NodeJS Package Manager ([npm](https://www.npmjs.com/)) should be installed so you could be able to build development environment on your local machine.
* Download `.zip` archive or clone the repository
* At this moment if user would like to change default startup settings he should change `config.properties` file in the directory: `effigy/effigy-backend- analyser/src/main/resources/`
* Open terminal and proceed to the copied folder `effigy`, next please use the following commands:
    * `$ cd effigy-backend-service/`
    * `$ npm install`
    * `$ grunt build`
    * `$ cd ..`
    * `$ gradle build` or `$ ./gradlew build`
* Next step is to fire up server by executing the following command in
 the terminal from the `effigy/effigy-backend-service` directory:
    * `$ java -jar build/libs/effigy-backend-service-1.0.jar`
    
After the last command the application should start its work. If everything configured properly it will automatically upload images, extract features, create Elasticsearch index and startup web application.
 Proceed to the `http://localhost:8080` to access working application.
