package pl.anim.image.search.analyser;

import org.elasticsearch.action.admin.indices.exists.indices.IndicesExistsRequest;
import org.elasticsearch.action.bulk.BulkRequestBuilder;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.IndicesAdminClient;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import pl.anim.image.search.analyser.intefaces.IndexBuilder;
import pl.anim.image.search.model.image.ImageRepresentation;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Properties;

import static org.elasticsearch.common.xcontent.XContentFactory.jsonBuilder;

/**
 * EffigyIndexBuilder implements {@link IndexBuilder} interface. It is a main entry point for the effigy-analyser module of the effigy project.
 * Main task of this class is prepossessing of everything connected with <b>Elasticsearch.</b>
 * The tasks include variety of checks on elasticsearch indexes, creation of a new index, population of index with images and features extraction from them.
 * At the same time, this class is a gateway to the processing part of the effigy-analyzer. Used by other modules of the project.
 *
 * @author Borys Komarov
 * @author Zahraa al-Taee
 *
 * @see IndexBuilder
 * @see ImagesAnalyser
 */

public class EffigyIndexBuilder implements IndexBuilder{

    public static void printGreeting(String greeting){
        System.out.println(greeting);
    }
    private final static String APPLICATION_PROPERTIES = "config.properties";
    private static Properties appProps = new Properties();

    /**
     * Variable responsible for storage name.
     * Initialy stored in config.properties file.
     */
    private static String bucketName;
    /**
     * Variable responsible for number of threads.
     * Initialy stored in config.properties file.
     */
    private static int numberOfThreads;

    static int getNumberOfThreads() {
        return numberOfThreads;
    }

    /**
     * Variable responsible for elasticserach cluster name.
     * Initialy stored in config.properties file.
     */
    private static String elasticClusterName;
    /**
     * Variable responsible for the list of elasticserach instances.
     * Initialy stored in config.properties file.
     */
    private static String elasticServers;
    /**
     * Variable responsible for elasticserach instances port number.
     * Initialy stored in config.properties file.
     */
    private static int elasticPort;
    /**
     * Variable responsible for elasticserach index name.
     * Initialy stored in config.properties file.
     */
    private static String elasticIndexName;
    /**
     * Variable responsible for elasticserach entity name.
     * Initialy stored in config.properties file.
     */
    private static String elasticEntity;

    public static String getElasticIndexName() {
        return elasticIndexName;
    }

    private static Client elasticClient;

    public static String parentPrefix;

    static {
        try {
            appProps.load(EffigyIndexBuilder.class.getClassLoader().getResourceAsStream(APPLICATION_PROPERTIES));
            bucketName = appProps.getProperty("cloud.bucket.name");

            numberOfThreads = Integer.valueOf((String) appProps.get("processing.numberOfThreads"));

            elasticClusterName = (String) appProps.get("elastic.name");
            elasticServers = (String) appProps.get("elastic.server");
            elasticPort = Integer.valueOf((String) appProps.get("elastic.port"));
            elasticIndexName = appProps.getProperty("elastic.index.name");
            elasticEntity = appProps.getProperty("elastic.index.entityName");
            elasticClient = createClient();

            parentPrefix = appProps.getProperty("parent.prefix");
        }
        catch (IOException e) {
            System.err.println("Problem with config.properties file!");
        }
    }

    /**
     * Field in which object responsible for different operations on images is held.
     *
     * @see ImagesAnalyser
     */
    private final ImagesAnalyser analyser;

    /**
     * Creates EffigyIndexBuilder with.
     */
    public EffigyIndexBuilder(){
        this.analyser = new ImagesAnalyser();
    }

    public boolean indexExists(){
        return checkIfIndexExists(elasticIndexName);
    }

    public boolean emptyIndex() {
        SearchResponse response = elasticClient.prepareSearch(elasticIndexName).execute().actionGet();
        return response.getHits().hits().length == 0;
    }

    public boolean createIndex() {
        IndicesAdminClient indicesAdminClient = elasticClient.admin().indices();
        indicesAdminClient.prepareCreate(elasticIndexName)
                            .addMapping(elasticEntity, "{\n" +
                                    "    \"image\":{\n" +
                                    "        \"properties\": {\n" +
                                    "                \"id\": {\n" +
                                    "                    \"type\": \"long\"\n" +
                                    "                },\n" +
                                    "               \"name\": {\n" +
                                    "                    \"type\": \"keyword\",\n" +
                                    "                    \"index\": true\n" +
                                    "               },\n" +
                                    "               \"url\": {\n" +
                                    "                    \"type\": \"text\",\n" +
                                    "                    \"index\": true\n" +
                                    "               },\n" +
                                    "               \"test\": {\n" +
                                    "                  \"type\": \"boolean\"\n" +
                                    "               },\n" +
                                    "               \"AUTO_COLOR_CORRELOGRAM\": {\n" +
                                    "                    \"type\": \"byte\"\n" +
                                    "               },\n" +
                                    "               \"EDGE_HISTOGRAM\": {\n" +
                                    "                    \"type\": \"byte\"\n" +
                                    "               },\n" +
                                    "               \"BINARY_PATTERNS_PYRAMID\": {\n" +
                                    "                    \"type\": \"byte\"\n" +
                                    "               },\n" +
                                    "               \"JOINT_HISTOGRAM\": {\n" +
                                    "                    \"type\": \"byte\"\n" +
                                    "               }\n" +
                                    "            }   \n" +
                                    "    }\n" +
                                    "}")
                            .get();

        return true;
    }

    public boolean deleteIndex(String indexName) {
        IndicesAdminClient indicesAdminClient = elasticClient.admin().indices();
        if(checkIfIndexExists(indexName)){
            indicesAdminClient.prepareDelete(indexName);
            return true;
        }
        return false;
    }

    private boolean checkIfIndexExists(String elasticIndexName) {
        return elasticClient.admin().indices().exists(new IndicesExistsRequest(elasticIndexName)).actionGet().isExists();
    }

    public void indexImageFeatures(ArrayList<ImageRepresentation> features) {
        System.out.println("Indexing images to elasticsearch!");
        BulkRequestBuilder bulkRequest = elasticClient.prepareBulk();
        int count = 0, bulkNumber = 1;
        for(ImageRepresentation image : features){
            try {
                bulkRequest.add(elasticClient.prepareIndex(elasticIndexName, "image", String.valueOf(image.getId()))
                            .setSource(jsonBuilder()
                                    .startObject()
                                    .field("name", image.getName())
                                    .field("url",image.getUrl())
                                    .field("test", image.getTested())
                                    .field("AUTO_COLOR_CORRELOGRAM",image.getFeatures().get(0))
                                    .field("EDGE_HISTOGRAM",image.getFeatures().get(1))
                                    .field("BINARY_PATTERNS_PYRAMID",image.getFeatures().get(2))
                                    .field("JOINT_HISTOGRAM",image.getFeatures().get(3))
                                    .endObject()
                            ));
                count++;
                if(count>100){
                    count = 0;
                    System.out.println("Sending a bulk!");
                    BulkResponse bulkResponse = bulkRequest.get();
                    if (bulkResponse.hasFailures()) {
                        System.out.println("HAS FAILURES IN BULK NUMBER" + bulkNumber);
                    }
                    bulkRequest = elasticClient.prepareBulk();
                    bulkNumber++;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        System.out.println("Sending FINAL bulk!");
        BulkResponse bulkResponse = bulkRequest.get();
        if (bulkResponse.hasFailures()) {
            System.err.println(bulkResponse.buildFailureMessage());
            System.out.println("\n HAS FAILURES IN BULK NUMBER" + bulkNumber);
        }
        System.out.println("Indexing is finished!");
    }

    public ArrayList<ImageRepresentation> extractImageFeatures() {
        return this.analyser.extractFeaturesFromBucket(bucketName);
    }


    /**
     * Static method which creates an elasticsearch client to communicate with elasticsearch instance.
     * Uses some static parameters defined during class initialization.
     *
     * @return instance of {@link Client} which is responsible for all communication with elasticsearch instance
     *
     * @see #elasticClusterName
     * @see #elasticServers
     * @see #elasticPort
     */
    public static Client createClient(){
        Settings settings = Settings.builder()
                .put("cluster.name", elasticClusterName)
                .build();
        TransportClient client = new PreBuiltTransportClient(settings);
        for (String server : elasticServers.split(",")) {
            try {
                client.addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName(server), elasticPort));
            } catch (UnknownHostException e) {
                e.printStackTrace();
            }
        }
        return client;
    }

}
