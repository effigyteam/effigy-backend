package pl.anim.image.search.analyser;



import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.services.storage.StorageScopes;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.storage.Blob;
import com.google.cloud.storage.Bucket;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;
import net.semanticmetadata.lire.imageanalysis.features.GlobalFeature;
import pl.anim.image.search.analyser.processing.Extractor;
import pl.anim.image.search.model.image.Features;
import pl.anim.image.search.model.image.ImageRepresentation;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import static pl.anim.image.search.analyser.EffigyIndexBuilder.getNumberOfThreads;

/**
 * ImageAnalyser class is responsible for features extraction from images.
 * The initial task is to get list of all images from the source, for example cloud storage.
 * Then extracts features using urls obtained from storage to access images. The extraction is parallel, number on threads depends on configuration
 * parameter {@link EffigyIndexBuilder#numberOfThreads}
 * It does so using instances of special processing unit called {@link Extractor}
 *
 * @author Borys Komarov
 * @author Zahraa al-Taee
 */
public class ImagesAnalyser {

    /**
     * Resulting ArrayList of {@link ImageRepresentation} to return.
     */
    public static ArrayList<ImageRepresentation> result;

    /**
     * Default constructor
     */
    ImagesAnalyser(){}

//    public void test(String bucketName){
//        Storage storage = StorageOptions.getDefaultInstance().getService();
//        // list a bucket's blobs
//        Bucket bucket = storage.get(bucketName);
//        if (bucket == null) {
//            System.out.println("No such bucket");
//            return;
//        }
//        Iterator<Blob> blobIterator = bucket.list().iterateAll();
//        while (blobIterator.hasNext()) {
//            Blob image = blobIterator.next();
//            System.out.println(image.getMediaLink());
//        }
//    }

    /**
     * Method which extracts vectors of image features using <a href = "http://www.lire-project.net/">LIRE</a> library.
     *
     * @param bucketName storage name to get images from
     * @return ArrayList of {@link ImageRepresentation} which represent vectors of features metadata for images
     */
    ArrayList<ImageRepresentation> extractFeaturesFromBucket(String bucketName) {
        result = new ArrayList<>();
        ArrayList<String> urls = null;
        try {
            urls = getListOfFiles(bucketName);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Extracting features from images! [BUCKET]");
        Extractor[] threads = new Extractor[getNumberOfThreads()];
        int chunk;
        if (urls != null && urls .size() > 0) {
            chunk = urls.size() / getNumberOfThreads();
        }
        else throw new RuntimeException("Problem with urls from bucket!");
        int stop = chunk;
        int start = 0;
        for(int j = 0; j < threads.length; j++){
            if(j == threads.length -1){
                if(stop > urls.size() || stop < urls.size()) stop = urls.size();
            }
            threads[j] = new Extractor(start, stop, urls);
            threads[j].start();

            stop+=chunk;
            start+=chunk;
        }
        for(Thread thread : threads){
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("Features were extracted! [BUCKET]");
        return result;
    }

    /**
     * Method which returns list of files in specified storage.
     * It is assumed that only images are stored there.
     *
     * @param bucketName storage name to get images from
     * @return ArrayList of Strings containing public urls of files in the storage
     */
    private ArrayList<String> getListOfFiles(String bucketName) throws IOException {
        ArrayList<String> urls = new ArrayList<>();
        InputStream inputStream = this.getClass().getResourceAsStream("/Effigy images-8eecdf538678.json");
        GoogleCredentials credentials = GoogleCredentials.fromStream(inputStream).createScoped(Arrays.asList(StorageScopes.DEVSTORAGE_READ_ONLY));
        Storage storage = StorageOptions.newBuilder().setProjectId("effigy-images").setCredentials(credentials).build().getService();
        // list bucket's blobs
        Bucket bucket = storage.get(bucketName);
        if (bucket == null) {
            System.out.println("No such bucket");
            return null;
        }
        Iterable<Blob> blobIterator = bucket.list().iterateAll();
        blobIterator.forEach(blob -> {
            String link = blob.getMediaLink();
            urls.add(link);
        });
        return urls;
    }

    /**
     * Hepler method user by core module effigy-backend-service to provide access to LIRE library.
     *
     * @param featureName required feature to extract
     * @param img image received from user
     * @return returns vector of particular image feature
     *
     * @see List
     */
    public static List<Integer> getFeature(String featureName, BufferedImage img){
        GlobalFeature f = null;
        List<Integer> feature = new ArrayList<>();
        Features featureEnum = Features.getByName(featureName);
        try {
            f = featureEnum.getFeatureClass().newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
        if (f != null) {
            f.extract(img);
        }
        else throw new RuntimeException("Feature class was not instantiated properly!");
        byte[] arr = f.getByteArrayRepresentation();
        for (byte anArr : arr) {
            feature.add((int) anArr);
        }
        return feature;
    }
}
