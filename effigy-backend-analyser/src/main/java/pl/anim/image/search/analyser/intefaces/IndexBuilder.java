package pl.anim.image.search.analyser.intefaces;

import pl.anim.image.search.model.image.ImageRepresentation;

import java.util.ArrayList;

/**
 * IndexBuilder interface is a general representation of a basic index builder for
 * such technologies as Elasticsearch or apache Lucene.
 * It includes the most basic functions which were needed during the development of effigy project.
 *
 * @author Borys Komarov
 * @author Zahraa al-Taee
 *
 * @see pl.anim.image.search.analyser.EffigyIndexBuilder
 */
public interface IndexBuilder {

    /**
     * Performs indexing of images into cluster.
     *
     * @param features is an array of the image representations which will be indexed
     */
    void indexImageFeatures(ArrayList<ImageRepresentation> features);

    /**
     * Extracts images features using the instance of {@link pl.anim.image.search.analyser.ImagesAnalyser} class.
     *
     * @return {@link ArrayList} of {@link ImageRepresentation}. Returns array of image representations ready to be indexed into elasticsearch
     */
    ArrayList<ImageRepresentation> extractImageFeatures();

    /**
     * Checks whether current index exists.
     *
     * @return <code>true</code> if the index exists,
     *         <code>false</code> otherwise
     */
    boolean indexExists();

    /**
     * Checks whether current index contains images or on.
     *
     * @return <code>true</code> if the index is empty,
     *         <code>false</code> otherwise
     */
    boolean emptyIndex();

    /**
     * Creates an index to store features and metadata extracted from images.
     *
     * @return <code>true</code> if the index was created successfully,
     *         <code>false</code> otherwise
     */
    boolean createIndex();

    /**
     * Deletes an index if it exists.
     * @param indexName name of
     * @return <code>true</code> if the index was deleted successfully,
     *         <code>false</code> otherwise
     */
    boolean deleteIndex(String indexName);
}
