package pl.anim.image.search.analyser.processing;

import net.semanticmetadata.lire.imageanalysis.features.GlobalFeature;
import pl.anim.image.search.analyser.EffigyIndexBuilder;
import pl.anim.image.search.model.image.Features;
import pl.anim.image.search.model.image.ImageRepresentation;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static pl.anim.image.search.analyser.ImagesAnalyser.result;

/**
 * Extractor is a class representing a processing unit used in effigy - analyser.
 * It directly uses API provided by <a href = "http://www.lire-project.net/">LIRE</a> library.
 *
 *
 * @author Borys Komarov
 *
 * @see pl.anim.image.search.analyser.ImagesAnalyser
 */
public class Extractor extends Thread {
    /**
     * Values representing start and stop indexes in the array of all public urls of images which current unit will use.
     */
    private int start, stop;

    /**
     * List of public urls to get images from
     */
    private ArrayList<String> urls;

    /**
     * Array of {@link GlobalFeature} to represent chosen features effigy project is working with.
     */
    private GlobalFeature[] features;

    /**
     * Constructor for Extractor class responsible for class initialization.
     *
     * @param start index in the urls array where current processing unit should start it's work
     * @param stop index in the urls array where current processing unit should stop it's work
     * @param urls list of public urls of images
     */
    public Extractor(int start, int stop, ArrayList<String> urls){
        this.start = start;
        this.stop = stop;
        this.urls = urls;
        createFeatureClasses("AUTO_COLOR_CORRELOGRAM", "EDGE_HISTOGRAM", "BINARY_PATTERNS_PYRAMID", "JOINT_HISTOGRAM");
    }

    /**
     * Method which creates a list of {@link GlobalFeature} out of the array of strings representing feature names.
     * @param featureNames an array of string storing image features names
     *
     * @see Features
     */
    private void createFeatureClasses(String ... featureNames) {
        features = new GlobalFeature[featureNames.length];
        for(int i = 0; i < featureNames.length; i++){
            GlobalFeature f = null;
            Features featureEnum = Features.getByName(featureNames[i]);
            try {
                f = featureEnum.getFeatureClass().newInstance();
            } catch (InstantiationException | IllegalAccessException e) {
                e.printStackTrace();
            }
            features[i] = f;
        }
    }

    public void run() {
        for(int i = start; i < stop; i++){
            BufferedImage img;
            String name;
            boolean tested = false;
            try {
                name = urls.get(i).substring(urls.get(i).indexOf("/o/") + 3, urls.get(i).lastIndexOf("?gen"));
                if(name.substring(0, EffigyIndexBuilder.parentPrefix.length()).equals(EffigyIndexBuilder.parentPrefix)){
                    tested = true;
                }
                img = ImageIO.read(new URL(urls.get(i)));
            } catch (IOException | IllegalArgumentException e) {
                System.out.println("Exception during image uploading! Skipping an image - " + i + ", url - " + urls.get(i));
                continue;
            }
            String imageName = "img - " + i;
            if(!name.equals("")) imageName = name;
            ImageRepresentation representation = generateImageRepresentation(img,i,imageName,urls.get(i), tested);
            if(representation != null)
                result.add(representation);
            System.out.println("Image with id - " + i + " and name - " + imageName + " processed.");
        }
    }

    /**
     * Method to extract features from given image. Intensively uses <a href = "http://www.lire-project.net/">LIRE</a> library.
     *
     * @param f object representing LIRE image feature
     * @param img image to extract features from
     * @param fileId image unique identification
     * @return vector representing particular feature for given image
     */
    private List<Integer> getFeature(GlobalFeature f, BufferedImage img, int fileId){
        List<Integer> feature = new ArrayList<>();
        try{
            f.extract(img);
        }catch (ArrayIndexOutOfBoundsException e){
            System.out.println("Exception during feature extraction! Skipping an image, id - " + fileId + ", url - " + urls.get(fileId));
            return null;
        }
        byte[] arr = f.getByteArrayRepresentation();
        for (byte anArr : arr) {
            feature.add((int) anArr);
        }
        return feature;
    }

    /**
     * Method which generates image representation object for further use.
     *
     * @param img image object
     * @param fileId image identification
     * @param name image name
     * @param url image public url
     * @return instance of {@link ImageRepresentation} object representing an image
     */
    private ImageRepresentation generateImageRepresentation(BufferedImage img, int fileId, String name, String url, boolean tested){
        List<List<Integer>> imageFeatures = new ArrayList<>();
        for (GlobalFeature feature : features) {
            List<Integer> f = getFeature(feature, img, fileId);
            if(f != null)
                imageFeatures.add(f);
            else return null;
        }
        return new ImageRepresentation(fileId, name, url, "Not applicable", imageFeatures, tested);
    }

}
