package pl.anim.image.search.model.authorization;

import java.util.Objects;

/**
 * Created by Borys on 1/16/17.
 */
public class Register {
    private String email;
    private String password;
    private int age;

    public String getEmail() {
        return email;
    }

    public void setAge(int age){
        this.age = age;
    }
    public int getAge(){
        return age;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public int hashCode() {
        return Objects.hash(email, password);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final Register other = (Register) obj;
        return Objects.equals(this.email, other.email)
                && Objects.equals(this.password, other.password);
    }
}
