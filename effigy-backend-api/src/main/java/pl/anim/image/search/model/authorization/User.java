package pl.anim.image.search.model.authorization;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.mongodb.core.mapping.Document;


import java.util.ArrayList;
import java.util.List;

/**
 * Created by Borys on 1/14/17.
 */
@Document(collection = "users")
@JsonIgnoreProperties(ignoreUnknown = true)
public class User{
    private final String email;
    private final String password;
    private final int age;
//    private final String token;
    List<String> role;

    @JsonCreator
    public User(@JsonProperty("email") String email,
                @JsonProperty("password") String password,
                @JsonProperty("role") String role,
                @JsonProperty("age") int age)
    {
        this.email = email;
        this.password = password;
        this.role = new ArrayList<>();
        this.role.add(role);
        this.age = age;
//        this.token = "";
    }

    @PersistenceConstructor
    public User(String email, String password, int age) {
        this.email = email;
        this.password = password;
        this.age = age;
        this.role = new ArrayList<>();
        this.role.add("USER");
    }
    public User(User u){
        this.email = u.getEmail();
        this.password = u.getPassword();
        this.role = u.getRole();
        this.age = u.getAge();
    }

    public User(Register registration) {
        this.email = registration.getEmail();
        this.password = registration.getPassword();
        this.age = registration.getAge();
//        this.token = token;
        this.role = new ArrayList<>();
        this.role.add("USER");
    }

    public String getEmail(){
        return email;
    }
    public String getPassword(){
        return password;
    }
    public List<String> getRole(){
        return role;
    }

    @Override
    public String toString() {
        return "User{" +
                ", email='" + email + '\'' +
                '}';
    }

    public int getAge() {
        return age;
    }
}