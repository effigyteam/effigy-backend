package pl.anim.image.search.model.authorization;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

public class UserAuthData {
	private final String username;
	private final String token;

	public UserAuthData(@JsonProperty("username") String username,
						@JsonProperty("token") String token) {
		this.username = username;
		this.token = token;
	}

	public String getUsername() {
		return username;
	}

	public String getToken() {
		return token;
	}

	@Override
	public int hashCode() {
		return Objects.hash(username, token);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}
		final UserAuthData other = (UserAuthData) obj;
		return Objects.equals(this.username, other.username)
				&& Objects.equals(this.token, other.token);
	}

	@Override
	public String toString() {
		return "UserAuthData{" +
				"username='" + username + '\'' +
				", token='" + token + '\'' +
				'}';
	}
}
