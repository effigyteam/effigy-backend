package pl.anim.image.search.model.error;

import java.util.Objects;

public class Error {

    private final String code;
    private final String message;
    private final String details;
    private final String path;
    private final String userMessage;

    private Error(String code, String message, String details, String path, String userMessage) {
        this.code = code;
        this.message = message;
        this.details = details;
        this.path = path;
        this.userMessage = userMessage;
    }

    public static Builder error() {
        return new Builder();
    }

    public static Builder error(String code) {
        return new Builder().withCode(code);
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public String getDetails() {
        return details;
    }

    public String getPath() {
        return path;
    }

    public String getUserMessage() {
        return userMessage;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Error)) {
            return false;
        }

        Error other = (Error) obj;
        return Objects.equals(code, other.code)
                && Objects.equals(path, other.path);
    }

    @Override
    public int hashCode() {
        return Objects.hash(code, path);
    }

    @Override
    public String toString() {
        return String.format("Error{code='%s', message='%s', details='%s', path='%s', userMessage='%s'}", code, message, details, path, userMessage);
    }

    public static final class Builder {

        private String code;
        private String message;
        private String details;
        private String path;
        private String userMessage;

        private Builder() {
        }

        public Error build() {
            return new Error(code, message, details, path, userMessage);
        }

        public Builder fromException(Throwable exception) {
            this.code = exception.getClass().getSimpleName();
            this.message = exception.getMessage();
            return this;
        }

        public Builder withCode(String code) {
            this.code = code;
            return this;
        }

        public Builder withMessage(String message) {
            this.message = message;
            return this;
        }

        public Builder withDetails(String details) {
            this.details = details;
            return this;
        }

        public Builder withUserMessage(String userMessage) {
            this.userMessage = userMessage;
            return this;
        }

        public Builder withPath(String path) {
            this.path = path;
            return this;
        }
    }
}

