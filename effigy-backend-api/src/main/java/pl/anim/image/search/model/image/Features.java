package pl.anim.image.search.model.image;

/**
 * Created by Borys on 2/26/17.
 */
import net.semanticmetadata.lire.imageanalysis.features.GlobalFeature;
import net.semanticmetadata.lire.imageanalysis.features.global.*;
import net.semanticmetadata.lire.imageanalysis.features.global.joint.*;
import net.semanticmetadata.lire.imageanalysis.features.global.centrist.*;
import net.semanticmetadata.lire.imageanalysis.features.global.spatialpyramid.*;

/**
 * Features supported by LIRE
 * Subclass of {@link GlobalFeature}
 */
public enum Features {

    AUTO_COLOR_CORRELOGRAM(AutoColorCorrelogram.class),
    BINARY_PATTERNS_PYRAMID(BinaryPatternsPyramid.class),
    CEDD(net.semanticmetadata.lire.imageanalysis.features.global.CEDD.class),
    EDGE_HISTOGRAM(EdgeHistogram.class),
    COLOR_LAYOUT(ColorLayout.class),
    FCTH(FCTH.class),
    FUZZY_COLOR_HISTOGRAM(FuzzyColorHistogram.class),
    FUZZY_OPPONENT_HISTOGRAM(FuzzyOpponentHistogram.class),
    GABOR(Gabor.class),
    JCD(JCD.class),
    JOINT_HISTOGRAM(JointHistogram.class),
    JPEG_COEFFICIENT_HISTOGRAM(JpegCoefficientHistogram.class),
    LOCAL_BINARY_PATTERNS(LocalBinaryPatterns.class),
    LOCAL_BINARY_PATTERNS_AND_OPPONENT(LocalBinaryPatternsAndOpponent.class),
    LUMINANCE_LAYOUT(LuminanceLayout.class),
    OPPONENT_HISTOGRAM(OpponentHistogram.class),
    PHOG(PHOG.class),
    RANK_AND_OPPONENT(RankAndOpponent.class),
    ROTATION_INVARIANT_LOCAL_BINARY_PATTERNS(RotationInvariantLocalBinaryPatterns.class),
    SCALABLE_COLOR(ScalableColor.class),
    SIMPLE_CENTRIST(SimpleCentrist.class),
    SIMPLE_COLOR_HISTOGRAM(SimpleColorHistogram.class),
    SPACC(SPACC.class),
    SPATIAL_PYRAMID_CENTRIST(SpatialPyramidCentrist.class),
    SPCEDD(SPCEDD.class),
    SPFCTH(SPFCTH.class),
    SPJCD(SPJCD.class),
    SPLBP(SPLBP.class),
    TAMURA(Tamura.class);

    /*
        EXAMPLE - featureEnum = FeatureEnum.getByName(parser.text());
                  lireFeature = featureEnum.getFeatureClass().newInstance();
                  GlobalFeature docFeature = GlobalFeature.getClass().newInstance();
    */
    private Class<? extends GlobalFeature> featureClass;

    Features(Class<? extends GlobalFeature> featureClass) {
        this.featureClass = featureClass;
    }

    public Class<? extends GlobalFeature> getFeatureClass() {
        return featureClass;
    }

    public static Features getByName(String name) {
        return valueOf(name.toUpperCase());
    }

}
