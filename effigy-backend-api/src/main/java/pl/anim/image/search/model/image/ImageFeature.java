package pl.anim.image.search.model.image;

public enum ImageFeature {
	AUTO_COLOR_CORRELOGRAM,
	BINARY_PATTERNS_PYRAMID,
	EDGE_HISTOGRAM,
	JOINT_HISTOGRAM,
}
