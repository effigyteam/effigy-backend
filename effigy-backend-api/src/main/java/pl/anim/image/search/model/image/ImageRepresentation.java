package pl.anim.image.search.model.image;



import java.util.List;

/**
 * Created by Borys on 12/10/16.
 */
public class ImageRepresentation {

    private final long id;
    private String url;
    private String name;
    private String path;
    private Boolean tested;
    private int numberOfEvaluations;

    private List<List<Integer>> features;

    public ImageRepresentation(int id, String name, String url, String path, List<List<Integer>> features, boolean tested){
        this.id = id;
        this.name = name;
        this .url = url;
        this.path = path;
        this.features = features;
        this.numberOfEvaluations=0;
        this.tested = tested;
    }

    public List<List<Integer>> getFeatures() {
        return features;
    }
    public long getId(){
        return id;
    }
    public String getName(){
        return name;
    }

    public String getUrl() {
        return url;
    }

    public String getPath() {
        return path;
    }

    public Boolean getTested() {
        return tested;
    }

    public int getNumberOfEvaluations() {
        return numberOfEvaluations;
    }
}
