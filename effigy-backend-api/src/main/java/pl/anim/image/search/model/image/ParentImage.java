package pl.anim.image.search.model.image;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ParentImage {
	private final String parentId;
	private final String imageUrl;

	@JsonCreator
	public ParentImage(@JsonProperty("parentId") String parentId,
					   @JsonProperty("imageUrl") String imageUrl) {
		this.parentId = parentId;
		this.imageUrl = imageUrl;
	}

	public String getParentId() {
		return parentId;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	@Override
	public String toString() {
		return "ParentImage{" +
				"parentId='" + parentId + '\'' +
				", imageUrl='" + imageUrl + '\'' +
				'}';
	}
}
