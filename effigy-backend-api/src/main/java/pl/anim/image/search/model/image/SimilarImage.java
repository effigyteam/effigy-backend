package pl.anim.image.search.model.image;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class SimilarImage {
	private final String imageId;
	private final String imageUrl;
	private final Integer ranking;

	@JsonCreator
	public SimilarImage(@JsonProperty("imageId") String imageId,
						@JsonProperty("imageUrl") String imageUrl,
						@JsonProperty("ranking") Integer ranking) {
		this.imageId = imageId;
		this.imageUrl = imageUrl;
		this.ranking = ranking;
	}

	public String getImageId() {
		return imageId;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public Integer getRanking() {
		return ranking;
	}

	@Override
	public String toString() {
		return "SimilarImage{" +
				"imageId='" + imageId + '\'' +
				", imageUrl='" + imageUrl + '\'' +
				", ranking=" + ranking +
				'}';
	}
}
