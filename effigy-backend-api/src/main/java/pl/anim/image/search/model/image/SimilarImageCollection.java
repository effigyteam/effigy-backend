package pl.anim.image.search.model.image;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Collections;
import java.util.List;

public class SimilarImageCollection {
	private final List<SimilarImage> similarImageList;

	@JsonCreator
	public SimilarImageCollection(@JsonProperty("similarImages") List<SimilarImage> similarImageList) {
		this.similarImageList = Collections.unmodifiableList(similarImageList);
	}

	@JsonProperty("similarImages")
	public List<SimilarImage> getSimilarImageList() {
		return similarImageList;
	}

	@Override
	public String toString() {
		return "SimilarImageCollection{" +
				"similarImageList=" + similarImageList +
				'}';
	}
}
