package pl.anim.image.search.model.score;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Borys on 1/16/17.
 */
public class AdvanceScoreCount {

        private final Integer number;
        private final String userName;
        @JsonCreator
        public AdvanceScoreCount(@JsonProperty("userName") String userName,
                                 @JsonProperty("number") Integer number) {
            this.number = number;
            this.userName = userName;
        }

        public Integer getScoreCount() {
            return number;
        }
        public String getUserName() {
        return userName;
    }

        @Override
        public String toString() {
            return "ScoreCount{" +
                    "scoreCount=" + number +
                    '}';
        }
}
