package pl.anim.image.search.model.score;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import pl.anim.image.search.model.image.SimilarImage;

import java.util.Collections;
import java.util.List;

/**
 * Created by Borys on 1/19/17.
 */
public class AdvanceScoreCountCollection {
    private final List<AdvanceScoreCount> topScores;
    private final Long userPosition;
    private final String userName;
    private final Long userScore;

    @JsonCreator
    public AdvanceScoreCountCollection(@JsonProperty("topScores") List<AdvanceScoreCount> topScores,
                                       @JsonProperty("userPosition") Long userPosition,
                                       @JsonProperty("userName") String userName,
                                       @JsonProperty("userScore") Long userScore) {
        this.topScores = Collections.unmodifiableList(topScores);
        this.userPosition = userPosition;
        this.userName = userName;
        this.userScore = userScore;
    }

    @JsonProperty("topScores")
    public List<AdvanceScoreCount> getSimilarImageList() {
        return topScores;
    }
    @JsonProperty("userPosition")
    public Long getUserPosition() {
        return userPosition;
    }
    @JsonProperty("userName")
    public String getUserName() {
        return userName;
    }
    @JsonProperty("userScore")
    public Long getUserScore() {
        return userScore;
    }

    @Override
    public String toString() {
        return "SimilarImageCollection{" +
                "similarImageList=" + topScores +
                '}';
    }
}
