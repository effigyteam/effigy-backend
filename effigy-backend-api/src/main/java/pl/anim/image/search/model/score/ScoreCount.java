package pl.anim.image.search.model.score;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ScoreCount {
	private final Integer scoreCount;

	@JsonCreator
	public ScoreCount(@JsonProperty("scoreCount") Integer scoreCount) {
		this.scoreCount = scoreCount;
	}

	public Integer getScoreCount() {
		return scoreCount;
	}

	@Override
	public String toString() {
		return "ScoreCount{" +
				"scoreCount=" + scoreCount +
				'}';
	}
}
