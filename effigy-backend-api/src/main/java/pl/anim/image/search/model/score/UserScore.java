package pl.anim.image.search.model.score;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.mongodb.core.mapping.Document;
import pl.anim.image.search.model.image.ImageFeature;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Document(collection = "user_scores")
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserScore {
	@Id
	private final String id;
	private final String parentId;
	private final String scoredImageId;
	private final Integer score;
	private final String username;
	private final List<ImageFeature> features;
    private final Integer ranking;

	@JsonCreator
	public UserScore(@JsonProperty("parentId") String parentId,
                     @JsonProperty("scoredImageId") String scoredImageId,
                     @JsonProperty("score") Integer score,
                     @JsonProperty("username") String username,
                     @JsonProperty("features") String features,
                     @JsonProperty("ranking") Integer ranking) {
        id = String.join(".", username, parentId, scoredImageId);
		this.parentId = parentId;
		this.scoredImageId = scoredImageId;
		this.score = score;
		this.username = username;
        this.features = Collections.unmodifiableList(
            Arrays.stream(features.split(","))
                .map(ImageFeature::valueOf)
                .collect(Collectors.toList())
        );
        this.ranking = ranking;
    }

    @PersistenceConstructor
    public UserScore(String parentId,
                     String scoredImageId,
                     Integer score,
                     String username,
                     List<ImageFeature> features,
                     Integer ranking) {
        id = String.join(".", username, parentId, scoredImageId);
        this.parentId = parentId;
        this.scoredImageId = scoredImageId;
        this.score = score;
        this.username = username;
        this.features = Collections.unmodifiableList(features);
        this.ranking = ranking;
    }

	public String getId() {
		return id;
	}

	public String getParentId() {
		return parentId;
	}

	public String getScoredImageId() {
		return scoredImageId;
	}

	public Integer getScore() {
		return score;
	}

	public String getUsername() {
		return username;
	}

    public List<ImageFeature> getFeatures() {
        return features;
    }

    public Integer getRanking() {
        return ranking;
    }

    @Override
    public String toString() {
        return "UserScore{" +
                "id='" + id + '\'' +
                ", parentId='" + parentId + '\'' +
                ", scoredImageId='" + scoredImageId + '\'' +
                ", score=" + score +
                ", username='" + username + '\'' +
                ", features=" + features +
                ", ranking=" + ranking +
                '}';
    }
}
