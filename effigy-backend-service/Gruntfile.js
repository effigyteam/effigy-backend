module.exports = function (grunt) {

    grunt.loadNpmTasks('grunt-karma');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-browserify');

    grunt.task.registerTask('test', ['karma:once']);
    grunt.task.registerTask('build', ['less', 'concat', 'browserify']);

    grunt.initConfig({
        assetsPath: 'src/main/front/',
        karma: {
            options: {
                configFile: 'karma.conf.js'
            },
            once: {
                singleRun: true,
                logLevel: 'ERROR'
            }
        },

        less: {
            prod: {
                options: {
                    compress: true,
                    yuicompress: true,
                    optimization: 2,
                    paths: '<%= assetsPath %>src/less/',
                    sourceMap: true,
                    sourceMapFilename: '<%= assetsPath %>dist/css/imagator.min.css.map',
                    sourceMapURL: '<%= assetsPath %>dist/css/imagator.min.css.map',
                    sourceMapRootpath: '<%= assetsPath %>src/less/',
                    sourceMapBasepath: '<%= assetsPath %>src/less/'
                },
                files: {
                    '<%= assetsPath %>dist/css/imagator.min.css': '<%= assetsPath %>src/less/index.less'
                }
            }
        },

        watch: {
            less: {
                files: ['<%= assetsPath %>src/less/**/*.less'],
                tasks: ['less']
            },
            browserify: {
                files: ['<%= assetsPath %>src/scripts/**/*.js'],
                tasks: ['browserify']
            }
        },

        concat: {
            libs: {
                src: [
                    'bower_components/angular/angular.min.js',
                    'bower_components/angular-ui-router/release/angular-ui-router.min.js',
                    'bower_components/angularjs-toaster/toaster.js',
                    'bower_components/angular-http-auth/src/http-auth-interceptor.js'
                ],
                dest: 'src/main/front/dist/js/libs.js'
            }
        },

        browserify: {
            dist: {
                src: ['src/main/front/src/scripts/app.js'],
                dest: 'src/main/front/dist/js/app.js'
            }
        }
    });
};
