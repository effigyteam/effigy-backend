module.exports = function(config) {
    config.set({
        basePath: '',
        frameworks: ['browserify', 'jasmine'],
        files: [
            'bower_components/angular/angular.js',
            'bower_components/angular-mocks/angular-mocks.js',

            'src/main/front/src/scripts/controller/imagesController.js',
            'src/main/front/spec/controller/imagesControllerSpec.js',

            'src/main/front/src/scripts/controller/loginController.js',
            'src/main/front/spec/controller/loginControllerSpec.js',

            'src/main/front/src/scripts/controller/scoresController.js',
            'src/main/front/spec/controller/scoresControllerSpec.js',

            'src/main/front/src/scripts/service/imagesService.js',
            'src/main/front/spec/service/imagesServiceSpec.js',

            'src/main/front/src/scripts/service/scoreService.js',
            'src/main/front/spec/service/scoreServiceSpec.js',

            'src/main/front/src/scripts/controller/userController.js',
            'src/main/front/spec/controller/userControllerSpec.js'
        ],

        exclude: [],

        // test results reporter to use
        // possible values: 'dots', 'progress', 'junit', 'growl', 'coverage'
        reporters: ['dots', 'coverage'],

        preprocessors: {
            'src/main/front/src/scripts/**/*.js': ['browserify'],
            'src/main/front/spec/**/*Spec.js': ['browserify'],
            'src/main/front/src/scripts/**/*.js': ['coverage']
        },

        coverageReporter: {
            type: 'lcov',
            dir: 'coverage'
        },

        // web server port
        port: 9876,

        // enable / disable colors in the output (reporters and logs)
        colors: true,

        // level of logging
        // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
        logLevel: config.LOG_INFO,

        // enable / disable watching file and executing tests whenever any file changes
        autoWatch: true,

        // Start these browsers, currently available:
        // - Chrome
        // - ChromeCanary
        // - Firefox
        // - Opera (has to be installed with `npm install karma-opera-launcher`)
        // - Safari (only Mac; has to be installed with `npm install karma-safari-launcher`)
        // - PhantomJS
        // - IE (only Windows; has to be installed with `npm install karma-ie-launcher`)
        browsers: ['PhantomJS'],

        // If browser does not capture in given timeout [ms], kill it
        captureTimeout: 60000,

        // Continuous Integration mode
        // if true, it capture browsers, run tests and exit
        singleRun: false,

        plugins : [
            'karma-jasmine',
            'karma-phantomjs-launcher',
            'karma-browserify',
            'karma-coverage'
        ]
    });
};