package pl.anim.image.search.effigy

import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.util.MimeType

class HttpHeadersBuilder {
    def HttpHeaders httpHeaders
    def List<MediaType> acceptMediaTypes

    def HttpHeadersBuilder() {
        httpHeaders = new HttpHeaders()
    }

    def add(String name, String value) {
        httpHeaders.add(name, value)
        this
    }

    def addContentType(String contentType) {
        def mediaType = contentType.split("/")
        httpHeaders.setAccept([new MimeType(mediaType[0], mediaType[1])])
        httpHeaders.setContentType(new MediaType(mediaType[0], mediaType[1]))
        this
    }

    def addAcceptHeader(String accept) {
        acceptMediaTypes = retrieveMediaTypesFromLine(accept)
        this
    }

    private static List<MediaType> retrieveMediaTypesFromLine(String accept) {
        accept.split(',').collect { String mediaTypeName ->
            MediaType.valueOf(mediaTypeName)
        }
    }

    def build() {
        if (acceptMediaTypes != null) {
            httpHeaders.setAccept(acceptMediaTypes)
        }

        httpHeaders
    }
}
