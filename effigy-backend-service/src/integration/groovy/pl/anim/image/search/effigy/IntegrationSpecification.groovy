package pl.anim.image.search.effigy

import com.github.tomakehurst.wiremock.junit.WireMockRule
import org.junit.ClassRule
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.context.web.WebAppConfiguration
import org.springframework.util.MultiValueMap
import spock.lang.Shared
import spock.lang.Specification

import static com.github.tomakehurst.wiremock.client.WireMock.reset

//@ContextConfiguration(loader = SpringApplicationContextLoader, classes = Start)
//@IntegrationTest("application.environment=integration")
//@ActiveProfiles("integration")
//@WebAppConfiguration
@ContextConfiguration
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
abstract class IntegrationSpecification extends Specification {

    private static final int WIREMOCK_PORT = 8099
    protected static final String LOCALHOST = "http://localhost:8080"

    @Shared
    @ClassRule
    def WireMockRule wireMockRule = new WireMockRule(WIREMOCK_PORT)

    TestRestTemplate template = new TestRestTemplate()
    def entity

    def setup() {
        reset()
    }

    def setEntity(HttpHeaders headers, String body) {
        entity = new HttpEntity<String>(body, headers);
        entity
    }

    def setFileEntity(HttpHeaders headers, MultiValueMap<String, Object> parts) {
        entity = new HttpEntity<MultiValueMap<String, Object>>(parts, headers)
        entity
    }

    def setEntity(HttpHeaders headers) {
        setEntity(headers, null)
    }

    def createHttpEntity(HttpHeaders headers, String body) {
        new HttpEntity<String>(body, headers)
    }

    def createHttpEntity(HttpHeaders headers) {
        createHttpEntity(headers, null)
    }

    def createHttpEntity(String contentType) {
        createHttpEntity(new HttpHeadersBuilder().addContentType(contentType).build(), null)
    }

    def createHttpEntity(String contentType, String body) {
        createHttpEntity(new HttpHeadersBuilder().addContentType(contentType).build(), body)
    }

    def createHttpEntityWithPermissions(String contentType, String request) {
        createHttpEntity(
            new HttpHeadersBuilder()
                .addContentType(contentType)
                .build(),
            request)
    }
}