package pl.anim.image.search.effigy.authorization

import org.springframework.http.HttpMethod
import pl.anim.image.search.effigy.IntegrationSpecification

class AuthorizationEndpointSpec extends IntegrationSpecification {

    def "should return 401 if token is invalid"() {
        def entity = createHttpEntity("application/json", """{"username": "name", "token": null}""")

        when:
        def result = template.exchange(LOCALHOST + "/verify", HttpMethod.POST, entity, Void)

        then:
        result.statusCode.value() == 401
    }
}
