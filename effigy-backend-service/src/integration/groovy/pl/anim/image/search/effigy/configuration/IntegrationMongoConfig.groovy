package pl.anim.image.search.effigy.configuration

import com.github.fakemongo.Fongo
import com.mongodb.Mongo
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile
import org.springframework.data.mongodb.config.AbstractMongoConfiguration
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories
import pl.anim.image.search.effigy.score.repository.ScoreRepository

@Configuration
@Profile("integration")
@EnableMongoRepositories(basePackageClasses = [ScoreRepository])
public class IntegrationMongoConfig extends AbstractMongoConfiguration {

    public static final String MONGO_DATABASE = "imagator-integration-test"

    @Override
    protected String getDatabaseName() {
        return MONGO_DATABASE
    }

    @Override
    public Mongo mongo() {
        return new Fongo(MONGO_DATABASE).getMongo()
    }

    @Override
    protected String getMappingBasePackage() {
        return "pl.anim.image.search.imagatorbackend.score.repository"
    }
}