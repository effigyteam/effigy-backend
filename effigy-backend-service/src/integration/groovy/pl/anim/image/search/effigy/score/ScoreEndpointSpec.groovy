package pl.anim.image.search.effigy.score

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpEntity
import org.springframework.http.HttpMethod
import org.springframework.http.MediaType
import pl.anim.image.search.model.image.ImageFeature
import pl.anim.image.search.model.score.ScoreCount
import pl.anim.image.search.model.score.UserScore
import pl.anim.image.search.effigy.IntegrationSpecification
import pl.anim.image.search.effigy.score.repository.ScoreRepository

class ScoreEndpointSpec extends IntegrationSpecification {

    @Autowired
    private final ScoreRepository repository

    def setup() {
        repository.deleteAll()
    }

    def "should save user score"() {
        given:
        def entity = createHttpEntity(MediaType.APPLICATION_JSON_VALUE,
            """{
                  "parentId": "parent_id",
                  "scoredImageId": "scored_id",
                  "score": 1,
                  "username": "firstname.lastname",
                  "ranking": 1,
                  "features": "PALETTE_POWER,FCTH"
               }""")

        when:
        def result = template.exchange(LOCALHOST + "/scores", HttpMethod.POST, entity, Void)

        then:
        result.statusCode.value() == 204
        repository.findOne("firstname.lastname.parent_id.scored_id").with {
            parentId == "parent_id";
            scoredImageId == "scored_id";
            score == 1;
            username == "firstname.lastname";
            ranking == 1;
            features == [ImageFeature.PALETTE_POWER, ImageFeature.FCTH]
        }
    }

    def "should get score count for the given user"() {
        given:
        def userScoreToFind = new UserScore("parent_id_1", "scored_id_1", 1, "firstname_1.lastname_1", "PALETTE_POWER", 1)
        def userScoreAdditional = new UserScore("parent_id_2", "scored_id_2", 1, "firstname_2.lastname_2", "PALETTE_POWER", 1)
        repository.save(userScoreToFind)
        repository.save(userScoreAdditional)

        when:
        def result = template.exchange(LOCALHOST + "/scores/count?username=firstname_1.lastname_1", HttpMethod.GET, entity as HttpEntity, ScoreCount)

        then:
        result.statusCode.value() == 200
        result.body.scoreCount == 1
    }
}
