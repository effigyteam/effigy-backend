var angular = require('angular');
var imagesController = require('../../src/scripts/controller/imagesController');

describe('imagesController', function(){

    var $scope, $controller, $rootScope,
        currentUser, verificationService, imagesService, scoreService;

    beforeEach(inject(function ($injector) {
        $rootScope = $injector.get('$rootScope');
        $controller = $injector.get('$controller');
        $scope = $rootScope.$new();

        currentUser = jasmine.createSpyObj('currentUser', ['getUser']);
        verificationService = jasmine.createSpyObj('verificationService', ['verifyUser']);
        scoreService = jasmine.createSpyObj('scoreService', ['sendScore'])
        imagesService = {
            'getSimilarImages': function () {
                return {
                    then: function () {}
                }
            },
            'getParentImage': function () {
                return {
                    then: function () {}
                }
            }
        };

        imageCtrl = $controller(imagesController, {
            $scope: $scope,
            currentUser: currentUser,
            verificationService: verificationService,
            imagesService: imagesService,
            scoreService: scoreService
        })
    }));

    describe('initialization', function () {
        it('should watch on model.parentImage and call "getSimilarImages" when changed', function () {
            $scope.model.parentImage = {};
            spyOn($scope, 'getSimilarImages');

            $scope.model.parentImage = {
                parentId: 'sss'
            };
            $scope.$digest();

            expect($scope.getSimilarImages).toHaveBeenCalled();
        });
    });

    describe('getSimilarImages', function () {
        beforeEach(function () {
            spyOn(imagesService, 'getSimilarImages').and.returnValue({
                then: function (promiseBind) {
                    promiseBind('was bound');
                }
            });
            spyOn($scope, 'setSimilarImages');
            
        });
        it('should call "getSimilarImages" on "imagesService"', function () {
            $scope.model.parentImage.parentId = 10;
            $scope.config.imagesQuantity = 5;

            $scope.getSimilarImages();

            expect(imagesService.getSimilarImages).toHaveBeenCalledWith(10, 5);
        });
        it('should not call "getSimilarImages" on "imagesService" if parentImage.parentId is undefined', function () {
            $scope.model.parentImage.parentId = undefined;

            $scope.getSimilarImages();

            expect(imagesService.getSimilarImages).not.toHaveBeenCalled();
        });
        it('should pass promise to "setSimilarImages"', function () {
            $scope.model.parentImage.parentId = 10;
            $scope.config.imagesQuantity = 5;

            $scope.getSimilarImages();

            expect($scope.setSimilarImages).toHaveBeenCalledWith('was bound');
        });
    });

    describe('getParentImage', function () {
        beforeEach(function () {
            spyOn(imagesService, 'getParentImage').and.returnValue({
                then: function (promiseBind) {
                    promiseBind('was bound');
                }
            });
            spyOn($scope, 'setParentImage');
        });
        it('should call "getParentImage" on "imagesService"', function () {
            $scope.getParentImage();

            expect(imagesService.getParentImage).toHaveBeenCalled();
        });
        it('should pass promise to "setParentImage"', function () {
            $scope.getParentImage();

            expect($scope.setParentImage).toHaveBeenCalledWith('was bound');
        });
    });
});