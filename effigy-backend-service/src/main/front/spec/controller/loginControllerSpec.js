var angular = require('angular');
var loginController = require('../../src/scripts/controller/loginController');

describe('loginController', function () {
    var $rootScope, $scope, loginService;

    beforeEach(inject(function ($injector) {
        $rootScope = $injector.get('$rootScope');
        $controller = $injector.get('$controller');
        $scope = $rootScope.$new();

        loginService = jasmine.createSpyObj('loginService', ['doLogin']);
        loginCtrl = $controller(loginController, {
            $scope: $scope,
            loginService: loginService
        })
    }));

    describe('login', function () {
        it('should call "doLogin" on "loginService" with $scope.data', function () {
            $scope.data = 'some data';

            $scope.login();

            expect(loginService.doLogin).toHaveBeenCalledWith('some data');
        });
    });
});