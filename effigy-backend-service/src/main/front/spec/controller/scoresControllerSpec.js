var angular = require('angular');
var scoresController = require('../../src/scripts/controller/scoresController');

describe('scoresController', function () {
    var $rootScope, $scope, $controller;

    beforeEach(inject(function ($injector) {
        $rootScope = $injector.get('$rootScope');
        $controller = $injector.get('$controller');
        $scope = $rootScope.$new();

        scoresCtrlr = $controller(scoresController, {
            $scope: $scope
        });

        $scope.helper.scoreLabels = ['bad', 'neutral', 'good'];
    }));

    describe('score', function () {
        it('should set helper.savedScore to passed value', function () {
            $scope.helper.savedScore = 0;

            $scope.score(2);

            expect($scope.helper.savedScore).toEqual(2);
        });
        it('should call "sendScore" with passed value', function () {
            spyOn($scope, 'sendScore');

            $scope.score(2);

            expect($scope.sendScore).toHaveBeenCalledWith(2);
        });
    });

    describe('changeCurrentScore', function () {
        it('should set helper.currentScore to passed value', function () {
            $scope.helper.currentScore = 1;

            $scope.changeCurrentScore(3);

            expect($scope.helper.currentScore).toEqual(3);
        });
        it('should set helper.label to corresponding index to value label', function () {
            $scope.helper.label = '';

            $scope.changeCurrentScore(2);

            expect($scope.helper.label).toEqual('good');
        });
    });

    describe('resetCurrentScore', function () {
        it('should reset current score back to saved score', function () {
            $scope.helper.savedScore = 1;
            $scope.helper.currentScore = 4;

            $scope.resetCurrentScore();

            expect($scope.helper.currentScore).toEqual(1);
        });
        it('should reset label to corresponding index label to saved score value', function () {
            $scope.helper.savedScore = 1;
            $scope.helper.label = 'bad';

            $scope.resetCurrentScore();

            expect($scope.helper.label).toEqual('neutral');
        });
    });

    describe('sendScore', function () {
        it('should emit event named "sendScore" passing value and $scope.image object', function () {
            $scope.image = {
                some: 'image'
            };
            spyOn($scope, '$emit');

            $scope.sendScore(2);

            expect($scope.$emit).toHaveBeenCalledWith('sendScore',2 , {
                some: 'image'
            });
        });
    });
});