var angular = require('angular');
var userController = require('../../src/scripts/controller/userController');

describe('userController', function () {
    var $rootScope, $scope, $controller, currentUser, scoreService;

    beforeEach(inject(function ($injector) {
        $controller = $injector.get('$controller');
        $rootScope = $injector.get('$rootScope');
        $scope = $rootScope.$new();

        currentUser = {
            getUser: function () {
                return {}
            }
        };
        scoreService = {
            getUserScoreCount: function () {
                return {
                    then: function () {}
                }
            }
        };

        userCtrl = $controller(userController, {
            $scope: $scope,
            currentUser: currentUser,
            scoreService: scoreService
        });
    }));

    describe('initalization', function () {
        it('should listen to broadcasted "updateScoreCount" event and bind getUserScoreCount to it', function () {
            spyOn($scope, 'getUserScoreCount');

            $rootScope.$broadcast('updateScoreCount');

            expect($scope.getUserScoreCount).toHaveBeenCalled();
        });
    });

    describe('getUserScoreCount', function () {
        it('should call "getUserScoreCount" on scoreService and bind "updateUserScoreCount" in $scope to returned promise', function () {
            spyOn(scoreService, 'getUserScoreCount').and.returnValue({
                then: function (promiseBind) {
                    promiseBind('was bound');
                }
            });
            spyOn($scope, 'updateUserScoreCount');

            $scope.getUserScoreCount();

            expect(scoreService.getUserScoreCount).toHaveBeenCalled();
            expect($scope.updateUserScoreCount).toHaveBeenCalledWith('was bound');
        });
    });

    describe('updateUserScoreCount', function () {
        it('should change userScoreCount to passed value', function () {
            $scope.model.userScoreCount = 2;

            $scope.updateUserScoreCount({
                scoreCount: 3
            });

            expect($scope.model.userScoreCount).toEqual(3);
        });
        it('should not set userScoreCount if it wasnt passed in data', function () {
            $scope.model.userScoreCount = 2;

            $scope.updateUserScoreCount({});

            expect($scope.model.userScoreCount).toEqual(2);
        });
    });
});