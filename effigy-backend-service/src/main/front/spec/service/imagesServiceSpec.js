var angular = require('angular');
var imagesService = require('../../src/scripts/service/imagesService');

describe('imagesService', function () {

    var service;

    beforeEach(inject(function ($http) {
        service = imagesService($http);
    }));

    describe('getRandomParentImage', function () {
        it('should return promise returning data from server response', inject(function ($httpBackend) {
            $httpBackend.expectGET('/images/parent/random')
                .respond(JSON.stringify({
                    parentId: 'someId'
                }));


            var promise = service.getRandomParentImage();
            $httpBackend.flush();

            expect(promise.$$state.value).toEqual({
                parentId: 'someId'
            });
        }));
    });

    describe('getSimilarImages', function () {
        it('should return promise returning data from images response route', inject(function ($httpBackend) {
            $httpBackend.expectGET('/images?parentId=SomeId&quantity=3')
                .respond(JSON.stringify({
                    similarImages: [{
                        imageId: 1
                    }, {
                        imageId: 2
                    }, {
                        imageId: 3
                    }]
                }));


            var promise = service.getSimilarImages('SomeId', 3);
            $httpBackend.flush();

            expect(promise.$$state.value).toEqual({
                similarImages: [{
                    imageId: 1
                }, {
                    imageId: 2
                }, {
                    imageId: 3
                }]
            });
        }));
    });
});