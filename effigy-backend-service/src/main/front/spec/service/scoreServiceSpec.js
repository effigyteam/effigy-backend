var angular = require('angular');
var scoreService = require('../../src/scripts/service/scoreService');

describe('scoreService', function () {

    var service;

    beforeEach(inject(function ($http) {
        service = scoreService($http);
    }));


    describe('sendScore', function () {
        it('should send passed data by post to /scores', inject(function ($http, $httpBackend) {
            spyOn($http, 'post').and.returnValue({
                then: function () {}
            });
            
            service.sendScore('somedata');

            expect($http.post).toHaveBeenCalledWith('/scores', 'somedata', {headers: {'Content-Type': 'application/json'}});
        }));
    });

    describe('getUserScoreCount', function () {
        it('should make a requst to /scores/count with username', inject(function ($http) {
            spyOn($http, 'get').and.returnValue({
                then: function () {}
            });

            service.getUserScoreCount('some.user');

            expect($http.get).toHaveBeenCalledWith('/scores/count?username=some.user');
        }));
        it('should return a promise returning information from the server', inject(function ($httpBackend) {
            $httpBackend.expectGET('/scores/count?username=some.user')
                .respond(JSON.stringify({
                    scoreCount: 2
                }));


            var promise = service.getUserScoreCount('some.user');
            $httpBackend.flush();

            expect(promise.$$state.value).toEqual({
                scoreCount: 2
            });
        }));
    });
});