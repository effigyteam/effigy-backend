(function () {
    var angular = require('angular'),
        app = angular.module('imagatorApp', [
            'ui.router',
            'toaster',
            'http-auth-interceptor'
        ]);

    app.config(['$urlRouterProvider', function($urlRouterProvider) {
        $urlRouterProvider.otherwise("/images");
    }]);

    app.config(function($stateProvider) {
        $stateProvider
            .state('images', {
                url: "/images?feature&parentId",
                templateUrl: "public/src/partials/images.html",
                controller: require('./controller/imagesController')
            })
            .state('login', {
                url: "/login",
                templateUrl: "public/src/partials/login.html",
                controller: require('./controller/loginController')
            })
            .state('logout', {
                url: "/logout",
                templateUrl: "public/src/partials/thank-you.html",
                controller: require('./controller/logoutController')
            });
    });

    app.config(function($httpProvider) {
        $httpProvider.interceptors.push(require('./interceptor/salusHeaderInterceptor'));
        $httpProvider.interceptors.push(require('./interceptor/errorInterceptor'));
    });

    app.factory('loginService', require('./service/loginService'));
    app.factory('logoutService', require('./service/logoutService'));
    app.factory('verificationService', require('./service/verificationService'));
    app.factory('currentUser', require('./service/currentUserService'));
    app.factory('imagesService', require('./service/imagesService'));
    app.factory('scoreService', require('./service/scoreService'));
    app.controller('scoresController', require('./controller/scoresController'));
    app.controller('userController', require('./controller/userController'));

    app.run(function ($rootScope) {

    });
})();
