module.exports = function imagesController($scope, $stateParams, currentUser,
                                           verificationService, imagesService, scoreService) {
    verificationService.verifyUser();

    $scope.config = {
        imagesQuantity: 4,
        feature: $stateParams["feature"],
        parentId: $stateParams["parentId"]
    };

    $scope.model = {
        parentImage: {},
        similarImages: []
    };

    $scope.isFeaturePresent = function () {
        return $stateParams["feature"] != undefined;
    };

    $scope.isParentIdPresent = function () {
        return $stateParams["parentId"] != undefined;
    };

    $scope.$watch('model.parentImage', function () {
        $scope.getSimilarImages();
    }, true);

    $scope.$on('sendScore', function (event, score, image) {
        var data = {
            parentId: $scope.model.parentImage.parentId,
            scoredImageId: image.imageId,
            score: score,
            username: currentUser.getUser().username || 'guest',
            ranking: image.ranking,
            features: $scope.isFeaturePresent() ? $scope.config.feature : "PALETTE_POWER"
        };
        scoreService.sendScore(data).then(function () {
            $scope.$broadcast('updateScoreCount');
        });
    });

    $scope.setParentImage = function (data) {
        $scope.model.parentImage = data;
    };

    $scope.setSimilarImages = function (data) {
        $scope.model.similarImages = data.similarImages;
    };

    $scope.getSimilarImages = function () {
        if ($scope.model.parentImage.parentId) {
            imagesService
                .getSimilarImages($scope.model.parentImage.parentId, $scope.config.imagesQuantity, $scope.config.feature)
                .then($scope.setSimilarImages);
        }
    };

    $scope.getParentImage = function () {
        if ($scope.isParentIdPresent()) {
            imagesService
                .getParentImageById($scope.config.parentId)
                .then($scope.setParentImage);
        } else {
            imagesService
                .getRandomParentImage()
                .then($scope.setParentImage);
        }
    };

    $scope.getParentImage();
};