module.exports = function loginController($scope, loginService) {
    $scope.login = function () {
        loginService.doLogin($scope.data);
    };
    $scope.register = function ($event) {
        loginService.doRegistration($scope.data);
    };
    $scope.onChangeTab = function ($event) {
        $event.preventDefault();
        loginService.changeTab($event);
    };
    $scope.textProc = function ($event) {
        loginService.textProc($event);
    };

};