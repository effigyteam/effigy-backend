module.exports = function logoutController($scope, logoutService, scoreService, currentUser) {
    $scope.scoreCount = 0;

    scoreService.getUserScoreCount(currentUser.getUser().username).then(function (data) {
        $scope.scoreCount = data.scoreCount;
    });

    logoutService.doLogout();
};