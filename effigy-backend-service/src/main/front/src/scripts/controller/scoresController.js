module.exports = function scoresController ($scope) {
    $scope.helper = {
        currentScore: 0,
        savedScore: 0,
        label: '',
        evalMethod: 2,
        scoreLabels: [
            '',
            'Strangers',
            'Friends',
            'Siblings',
            'Twins',
            'Identical'
        ]
    };

    $scope.score = function (value, $event) {
        $event.preventDefault();
        $scope.helper.savedScore = value;
        $scope.sendScore(value);
    };
    $scope.changeCurrentScore = function (value) {
        $scope.helper.label = $scope.helper.scoreLabels[value];
        $scope.helper.currentScore = value;
    };
    $scope.resetCurrentScore = function () {
        $scope.helper.currentScore = $scope.helper.savedScore;
        $scope.helper.label = $scope.helper.scoreLabels[$scope.helper.savedScore];
    };
    $scope.sendScore = function (value) {
        $scope.$emit('sendScore', value, $scope.image);
    }
};