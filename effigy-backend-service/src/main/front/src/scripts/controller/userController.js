module.exports = function userController ($scope, currentUser, scoreService) {

    $scope.model = {
        userScoreCount: 0
    };

    $scope.$on('updateScoreCount', function () {
        $scope.getUserScoreCount();
    });

    $scope.getUserScoreCount = function () {
        var username = currentUser.getUser().username || 'guest';

        scoreService
            .getUserScoreCount(username)
            .then($scope.updateUserScoreCount)
    };

    $scope.updateUserScoreCount = function (data) {
        if (data.scoreCount) {
            $scope.model.userScoreCount = data.scoreCount;
        }
    };

    $scope.getUserScoreCount();
};