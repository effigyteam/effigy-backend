function errorInterceptor($q, $injector) {
    return {
        'responseError': function (rejection) {
            var defer = $q.defer();

            if (rejection.status == 401) {
                $injector.get("$state").transitionTo("login");
            }

            if (rejection.status == 404) {
                $injector.get("$state").transitionTo("logout");
            }

            console.dir(rejection);

            defer.reject(rejection);

            return defer.promise;
        }
    };
}

module.exports = errorInterceptor;