function salusHeaderInterceptor(currentUser) {
    return {
        request: function(config) {
            var token = currentUser.getToken();
            if (token) {
                config.headers['Authorization'] = token;
            }
            return config;
        }
    };
}

module.exports = salusHeaderInterceptor;