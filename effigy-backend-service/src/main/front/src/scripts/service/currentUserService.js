module.exports = function currentUser() {
    var storage = window.localStorage,
        KEY = 'user';

    return {
        setUser: function (userData) {
            storage.setItem(KEY, JSON.stringify(createUser(userData)))
        },
        drop: function () {
            storage.removeItem(KEY);
        },
        getUser: function () {
            return JSON.parse(storage.getItem(KEY)) || createUser();
        },
        getToken: function () {
            return this.getUser().token;
        }

    }
};

function createUser(optionalData) {
    var data = optionalData || {};
    return {
        username: data.username || null,
        token: data.token || null
    }
}