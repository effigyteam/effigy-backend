module.exports = function imagesService ($http, currentUser) {
    var service = {};

    service.getRandomParentImage = function () {
        var username = currentUser.getUser().username;
        var promise = $http.get('/images/parent/random?username=' + username).then(function (response) {
            return response.data;
        });
        return promise
    };

    service.getParentImageById = function (parentId) {
        var promise = $http.get('/images/parent/' + parentId).then(function (response) {
            return response.data;
        });

        return promise;
    };

    service.getSimilarImages = function (parentId, quantity, feature) {
        var url;
        if (feature != undefined) {
            url = '/images/' + feature + '?parentId=' + parentId + '&quantity=' + quantity;
        } else {
            url = '/images?parentId=' + parentId + '&quantity=' + quantity;
        }
        var promise = $http.get(url).then(function (response) {
            return response.data;
        });
        return promise
    };

    return service;
};