function loginServiceFactory($http, $rootScope, $state, currentUser) {
    var service = {};

    service.doLogin = function (loginData) {
        //var str = "username="+loginData.username+"&password="+loginData.password;
        return $http({
            data: loginData,
            url: '/login',
            method: 'post'
        }).then(function (response) {
            if(response.data == ""){
                alert("Wrong login or password data!");
            }
            else{
                var f = "AUTO_COLOR_CORRELOGRAM,EDGE_HISTOGRAM";
                currentUser.setUser(response.data);
                $state.go('images' , { feature: f });
            }
        }, function errorCallback(response) {
            console.log("Login error");
        });
    };

    service.doRegistration = function(data){
        return $http({
            data: data,
            url: '/register',
            method: 'post'
        }).then(function (response) {
            if(response.data == ""){
                alert("User with such email exists!");
            }
            else{
                currentUser.setUser(response.data);
                $state.go('images');
            }
        }, function errorCallback(response) {
           console.log("Registration error");
        });
    };
    service.changeTab = function($event){
        var th = $($event.target);
        th.parent().addClass('active');
        th.parent().siblings().removeClass('active');

        var toShow = th.attr('href');

        $('.tab-content > div').not(toShow).hide();

        $(toShow).fadeIn(600);
    };
    service.textProc = function($event){
        var th = $($event.target);
        var $this = $($event.target),
            label = $this.prev('label');

        if ($event.type === 'keyup') {
            if ($this.val() === '') {
                label.removeClass('active highlight');
            } else {
                label.addClass('active highlight');
            }
        } else if ($event.type === 'blur') {
            if( $this.val() === '' ) {
                label.removeClass('active highlight');
            } else {
                label.removeClass('highlight');
            }
        } else if ($event.type === 'focus') {

            if( $this.val() === '' ) {
                label.removeClass('highlight');
            }
            else if( $this.val() !== '' ) {
                label.addClass('highlight');
            }
        }
    };

    return service;
}

module.exports = loginServiceFactory;