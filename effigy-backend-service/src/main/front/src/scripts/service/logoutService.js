function logoutServiceFactory($http, $rootScope, $state, currentUser) {
    var service = {};

    service.doLogout = function () {
        return $http({
            data: currentUser.getUser(),
            headers: {'Content-Type': 'application/json'},
            url: '/logout',
            method: 'post'
        }).then(function (response) {
            currentUser.drop();
        });
    };

    return service;
}

module.exports = logoutServiceFactory;