module.exports = function scoreService ($http) {
    var service = {};

    service.sendScore = function (data) {
        var headers = {
            'Content-Type': 'application/json'
        };
        var promise = $http.post('/scores', data, { headers: headers });
        return promise;
    };

    service.getUserScoreCount = function (username) {
        var promise = $http.get('/scores/count?username=' + username)
            .then(function (response) {
                return response.data;
            });
        return promise;
    };

    return service;
};