function verificationServiceFactory($http, $rootScope, $state, currentUser) {
    var service = {};

    service.verifyUser = function () {
        return $http({
            data: currentUser.getUser(),
            url: '/verify',
            method: 'post'
        });
    };

    return service;
}

module.exports = verificationServiceFactory;