package pl.anim.image.search.effigy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import pl.anim.image.search.effigy.authorization.exception.EmptyTokenException;
import pl.anim.image.search.effigy.authorization.exception.PermissionDeniedException;
import pl.anim.image.search.effigy.authorization.exception.UserUnauthorizedException;
import pl.anim.image.search.effigy.image.exception.FullCycleOfParentImagesIsOver;
import pl.anim.image.search.effigy.image.exception.ParentImageNotFoundException;
import pl.anim.image.search.model.error.Errors;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.NotAuthorizedException;
import java.io.PrintWriter;
import java.io.StringWriter;

@ControllerAdvice
public class ErrorHandler {
	private final static Logger logger = LoggerFactory.getLogger(ErrorHandler.class);

	@Value("${error.details}")
	private Boolean showErrorDetails;

	@ExceptionHandler(PermissionDeniedException.class)
	@ResponseStatus(value = HttpStatus.UNAUTHORIZED)
	@ResponseBody
	public Errors handlePermissionDeniedException(HttpServletRequest request, Exception exception) {
		logger.warn(exception.getMessage(), exception);
		return buildErrorWithMessage(request, exception, exception.getClass().getSimpleName());
	}

	@ExceptionHandler(UserUnauthorizedException.class)
	@ResponseStatus(value = HttpStatus.UNAUTHORIZED)
	@ResponseBody
	public Errors handleUserUnauthorizedException(HttpServletRequest request, Exception exception) {
		logger.warn(exception.getMessage(), exception);
		return buildErrorWithMessage(request, exception, exception.getClass().getSimpleName());
	}

    @ExceptionHandler(EmptyTokenException.class)
    @ResponseStatus(value = HttpStatus.UNAUTHORIZED)
    @ResponseBody
    public Errors handleEmptyTokenException(HttpServletRequest request, Exception exception) {
        logger.warn(exception.getMessage(), exception);
        return buildErrorWithMessage(request, exception, exception.getClass().getSimpleName());
    }

	@ExceptionHandler(NotAuthorizedException.class)
	@ResponseStatus(value = HttpStatus.UNAUTHORIZED)
	@ResponseBody
	public Errors handleNotUnauthorizedException(HttpServletRequest request, Exception exception) {
		logger.warn(exception.getMessage(), exception);
		return buildErrorWithMessage(request, exception, exception.getClass().getSimpleName());
	}

	@ExceptionHandler(ParentImageNotFoundException.class)
	@ResponseStatus(value = HttpStatus.NOT_FOUND)
	@ResponseBody
	public Errors handleParentImageNotFoundException(HttpServletRequest request, Exception exception) {
		logger.warn(exception.getMessage(), exception);
		return buildErrorWithMessage(request, exception, exception.getClass().getSimpleName());
	}
	@ExceptionHandler(FullCycleOfParentImagesIsOver.class)
	@ResponseStatus(value = HttpStatus.NOT_FOUND)
	@ResponseBody
	public Errors handleFullCycleOfParentImagesIsOver(HttpServletRequest request, Exception exception) {
		logger.warn(exception.getMessage(), exception);
		return buildErrorWithMessage(request, exception, exception.getClass().getSimpleName());
	}

	private Errors buildErrorWithMessage(HttpServletRequest request, Exception exception, String errorStringCode) {
		return new Errors().addError(
			pl.anim.image.search.model.error.Error.error(errorStringCode)
				.withPath(request.getRequestURI())
				.withDetails(showErrorDetails ? getStackTrace(exception) : null)
				.withMessage(exception.getMessage())
				.withUserMessage(exception.getLocalizedMessage())
				.build()
		);
	}

	private String getStackTrace(Exception exception) {
		StringWriter stringWriter = new StringWriter();
		exception.printStackTrace(new PrintWriter(stringWriter));
		return stringWriter.toString();
	}
}
