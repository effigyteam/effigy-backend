package pl.anim.image.search.effigy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import pl.anim.image.search.analyser.EffigyIndexBuilder;
import pl.anim.image.search.analyser.intefaces.IndexBuilder;

/**
 *
 * Start class is responsible for initial elasticsearch index set up, effigy-analyser initialization
 * and the startup of the effigy-backend-service module on the embedded tomcat server.
 *
 * @author Borys Komarov
 * @author Zahraa al-Taee
 */
@SpringBootApplication
public class Start {

	/**
	 * Main function which is an entry point to the whole effigy project.
     *
	 * @param args command line arguments
	 */
	public static void main(String[] args) {

		EffigyIndexBuilder.printGreeting("Analyser in da town");

		IndexBuilder builder = new EffigyIndexBuilder();
		if(builder.indexExists()){
			if(builder.emptyIndex()){
				builder.indexImageFeatures(builder.extractImageFeatures());
			}
		}
		else{
			if(builder.createIndex()){
				builder.indexImageFeatures(builder.extractImageFeatures());
			}
		}
		System.out.println("Starting Spring Boot application!");
		SpringApplication.run(Start.class, args);
	}
}
