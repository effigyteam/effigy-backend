package pl.anim.image.search.effigy.authorization.controller;


import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import pl.anim.image.search.effigy.authorization.service.UserAuthorizationService;
import pl.anim.image.search.model.authorization.Login;
import pl.anim.image.search.model.authorization.Register;
import pl.anim.image.search.model.authorization.UserAuthData;

import javax.servlet.http.HttpServletRequest;

@RestController
public class AuthorizationEndpoint {

	private UserAuthorizationService userAuthorizationService;

	@Value("${user.session.identifier}")
	private String userIdentifier;

	@Autowired
	public AuthorizationEndpoint(UserAuthorizationService userAuthorizationService) {
		this.userAuthorizationService = userAuthorizationService;
	}

	@RequestMapping(value = "/login", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(value = "Log in")
	public UserAuthData login(HttpServletRequest request, @RequestBody Login login) {
		UserAuthData data = userAuthorizationService.authorize(login);
		return data;
	}

	@RequestMapping(value = "/register", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(value = "Log in")
	public UserAuthData login(HttpServletRequest request, @RequestBody Register registration) {
		UserAuthData data = userAuthorizationService.register(registration);
		return data;
	}

	@RequestMapping(value = "/verify", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(value = "Verify token")
	public void verifyToken(HttpServletRequest request, @RequestBody UserAuthData userAuthData) {
//		if(request.getSession().getAttribute("user")==null){
//			throw new PermissionDeniedException("Authorization token is not valid");
//		}
 		userAuthorizationService.checkToken(userAuthData.getToken());
//		SecurityContext ctx = SecurityContextHolder.getContext();
//		Authentication authentication2 = ctx.getAuthentication();
//		authentication2.setAuthenticated(true);
//		User custom = authentication == null ? null : (User) authentication.getPrincipal();
//		if(custom == null) throw new PermissionDeniedException("Authorization token is not valid");
//		int i=0;
	}

	@RequestMapping(value = "/logout", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(value = "Log out")
	public void logout(HttpServletRequest request, @RequestBody UserAuthData userAuthData) {
		request.getSession().setAttribute(userIdentifier, null);
	}
}
