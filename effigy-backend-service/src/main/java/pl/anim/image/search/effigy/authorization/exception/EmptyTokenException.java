package pl.anim.image.search.effigy.authorization.exception;

public class EmptyTokenException extends RuntimeException {
	public EmptyTokenException(String message) {
		super(message);
	}
}
