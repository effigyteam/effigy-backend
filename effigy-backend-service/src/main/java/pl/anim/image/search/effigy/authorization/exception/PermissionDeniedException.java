package pl.anim.image.search.effigy.authorization.exception;

public class PermissionDeniedException extends RuntimeException {
	public PermissionDeniedException(String message) {
		super(message);
	}

	public PermissionDeniedException(Throwable e) {
		super(e);
	}
}
