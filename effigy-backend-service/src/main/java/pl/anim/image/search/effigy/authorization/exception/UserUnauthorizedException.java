package pl.anim.image.search.effigy.authorization.exception;

public class UserUnauthorizedException extends RuntimeException {
	public UserUnauthorizedException(Throwable e) {
		super(e);
	}
}
