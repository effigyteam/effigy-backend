package pl.anim.image.search.effigy.authorization.repository;

import org.springframework.data.repository.CrudRepository;
import pl.anim.image.search.model.authorization.User;

/**
 * Created by Borys on 1/14/17.
 */
public interface UserRepository extends CrudRepository<User, String > {
    User findByEmail(String username);
}
