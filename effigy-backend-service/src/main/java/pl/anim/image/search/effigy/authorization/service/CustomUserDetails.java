package pl.anim.image.search.effigy.authorization.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import pl.anim.image.search.effigy.authorization.repository.UserRepository;

import java.util.Collection;

/**
 * Created by Borys on 1/14/17.
 */

public class CustomUserDetails extends pl.anim.image.search.model.authorization.User implements UserDetails {

    @Autowired
    private UserRepository userRepository;

    public CustomUserDetails(pl.anim.image.search.model.authorization.User u) {
        super(u);
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return AuthorityUtils.createAuthorityList("ROLE_USER");
    }

    @Override
    public String getUsername() {
        return this.getEmail();
    }

    @Override
    public boolean isAccountNonExpired() {
        return false;
    }

    @Override
    public boolean isAccountNonLocked() {
        return false;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return false;
    }

    @Override
    public boolean isEnabled() {
        return false;
    }

//    @Override
//    @Transactional(readOnly = true)
//    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
//        pl.anim.image.search.model.authorization.User user = userRepository.findByUsername(username);
//
//        Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
//        for (String role : user.getRole()){
//            grantedAuthorities.add(new SimpleGrantedAuthority(role));
//        }
//
//        return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), grantedAuthorities);
//    }
}
