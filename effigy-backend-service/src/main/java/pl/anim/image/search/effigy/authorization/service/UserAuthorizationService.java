package pl.anim.image.search.effigy.authorization.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import pl.anim.image.search.effigy.authorization.exception.PermissionDeniedException;
import pl.anim.image.search.effigy.authorization.exception.UserUnauthorizedException;
import pl.anim.image.search.effigy.authorization.repository.UserRepository;
import pl.anim.image.search.model.authorization.Login;
import pl.anim.image.search.model.authorization.Register;
import pl.anim.image.search.model.authorization.User;
import pl.anim.image.search.model.authorization.UserAuthData;

import static pl.anim.image.search.effigy.utils.HelperMethods.randomString;


@Component
public class UserAuthorizationService {

	private final UserRepository userRepository;
	private final PasswordEncoder encoder;

	@Autowired
	public UserAuthorizationService(UserRepository userRepository, PasswordEncoder encoder) {
		this.userRepository = userRepository;
		this.encoder = encoder;
	}

	public UserAuthData authorize(Login login) {
		User user = userRepository.findByEmail(login.getUsername());
		if(user != null){
			if(encoder.matches(login.getPassword(),user.getPassword())){
				try {
					return new UserAuthData(login.getUsername(), randomString(12));
				} catch ( IllegalArgumentException e) {
					throw new UserUnauthorizedException(e);
				}
			} else return null;
		} else return null;
	}

	public void checkToken(String token) {
		if (token == null) {
			throw new PermissionDeniedException("Authorization token is not valid");
		}
	}

	public UserAuthData register(Register registration) {
		String token = randomString(12);
		registration.setPassword(encoder.encode(registration.getPassword()));
		User user = userRepository.findByEmail(registration.getEmail());
		if(user == null){
			User nUser = new User(registration);
			userRepository.save(nUser);
			try {
				return new UserAuthData(registration.getEmail(), token);
			} catch ( IllegalArgumentException e) {
				throw new UserUnauthorizedException(e);
			}
		}
		else return null;
	}
}
