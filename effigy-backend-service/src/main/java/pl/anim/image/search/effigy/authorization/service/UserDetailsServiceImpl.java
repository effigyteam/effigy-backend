package pl.anim.image.search.effigy.authorization.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import pl.anim.image.search.effigy.authorization.repository.UserRepository;

/**
 * Created by Borys on 1/14/17.
 */
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        pl.anim.image.search.model.authorization.User user = userRepository.findByEmail(username);
        if(user == null){
            throw new UsernameNotFoundException("No such user found!");
        }
        return new CustomUserDetails(user);
    }
}
