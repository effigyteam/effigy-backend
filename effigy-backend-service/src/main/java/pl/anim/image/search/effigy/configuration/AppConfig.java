package pl.anim.image.search.effigy.configuration;

import com.github.jknack.handlebars.springmvc.HandlebarsViewResolver;
import com.mongodb.MongoClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import pl.anim.image.search.effigy.score.repository.ScoreRepository;
import pl.anim.image.search.effigy.utils.AggregationQueriesHelper;

@Configuration
@EnableWebMvc
public class AppConfig extends WebMvcConfigurerAdapter {

	@Value("${spring.data.mongodb.database}")
	private String dbName;

	@Bean
	public ViewResolver viewResolver() {
		HandlebarsViewResolver viewResolver = new HandlebarsViewResolver();
		viewResolver.setPrefix("classpath:templates");
		viewResolver.setSuffix(".hbs");
		return viewResolver;
	}

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/public/**/*.*").addResourceLocations("classpath:front/");
	}

	@Bean
    @Autowired
	public AggregationQueriesHelper aggregationQueriesHelper(MongoClient mongo, ScoreRepository scoreRepository){
		return new AggregationQueriesHelper(mongo, dbName, scoreRepository);
	}

}
