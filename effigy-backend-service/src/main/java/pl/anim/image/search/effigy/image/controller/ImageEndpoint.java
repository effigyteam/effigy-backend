package pl.anim.image.search.effigy.image.controller;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import pl.anim.image.search.effigy.image.exception.FullCycleOfParentImagesIsOver;
import pl.anim.image.search.effigy.image.exception.ParentImageNotFoundException;
import pl.anim.image.search.effigy.image.service.ImageService;
import pl.anim.image.search.model.image.ImageFeature;
import pl.anim.image.search.model.image.SimilarImageCollection;
import pl.anim.image.search.model.image.ParentImage;

/**
 * ImageEndpoint is a REST controller responsible for all server actions connected with images.
 */
@RestController
@RequestMapping(value = "/images")
public class ImageEndpoint {

	private final ImageService imageService;

	/**
	 * Constructor for ImageEndpoint.
	 *
	 * @param imageService {@link ImageService} instance responsible for logic and database interactions
	 */
	@Autowired
	public ImageEndpoint(ImageService imageService) {
		this.imageService = imageService;
	}

	/**
	 * Returns an object which corresponds to the random image.
	 *
	 * @return {@link ParentImage} instance which contains information about the image which was retrieved
	 */
	@RequestMapping(value = "/parent/random", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(value = "Get random parent image")
	public ParentImage getRandomParentImage(@RequestParam(value = "username") String username) throws ParentImageNotFoundException, FullCycleOfParentImagesIsOver {
	    return imageService.getRandomParentImage(username);
	}

	/**
	 * Method which returns list of similar images.
	 *
	 * @param parentId identification of image for which we should find similar images
	 * @param quantity number of similar images to return
	 * @return collection of similar images
	 */
	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(value = "Get similar images")
	public SimilarImageCollection getSimilarImages(@RequestParam(value = "parentId") String parentId,
												   @RequestParam(value = "quantity", required = false) Integer quantity) {
		return imageService.getSimilarImages(parentId, quantity);
	}

	/**
	 * Method which returns list of similar images using exact given feature to evaluate.
	 *
	 * @param parentId identification of image for which we should find similar images
	 * @param imageFeatures list of features to use
	 * @param quantity number of similar images to return
	 * @return collection of similar images
	 */
	@RequestMapping(value = "/{features}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(value = "Get similar images based on the specified feature")
	public SimilarImageCollection getSimilarImagesByFeature(@RequestParam(value = "parentId") String parentId,
															@PathVariable(value = "features") ImageFeature[] imageFeatures,
															@RequestParam(value = "quantity", required = false) Integer quantity) {
		return imageService.getSimilarImagesByFeature(parentId, imageFeatures, quantity);
	}

	/**
	 * Method which returns similar images to the image which user provides
	 *
	 * @param image file received from user to find similar images
	 * @return collection of similar images
	 */
	@RequestMapping(value = "/file",method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(value = "Get similar images to user image")
	public SimilarImageCollection getSimilarToUserImages(@RequestParam(value = "image") MultipartFile image) {
		return imageService.getImageFromUser(image);
	}

}
