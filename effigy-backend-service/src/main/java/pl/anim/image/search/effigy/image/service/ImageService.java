package pl.anim.image.search.effigy.image.service;

import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.common.lucene.search.function.CombineFunction;
import org.elasticsearch.index.query.*;
import org.elasticsearch.index.query.functionscore.ScoreFunctionBuilders;
import org.elasticsearch.index.query.functionscore.ScriptScoreFunctionBuilder;
import org.elasticsearch.script.Script;
import org.elasticsearch.script.ScriptType;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import pl.anim.image.search.analyser.EffigyIndexBuilder;
import pl.anim.image.search.analyser.ImagesAnalyser;
import pl.anim.image.search.effigy.image.exception.FullCycleOfParentImagesIsOver;
import pl.anim.image.search.effigy.image.exception.ParentImageNotFoundException;
import pl.anim.image.search.effigy.score.repository.ScoreRepository;
import pl.anim.image.search.model.image.ImageFeature;
import pl.anim.image.search.model.image.ParentImage;
import pl.anim.image.search.model.image.SimilarImage;
import pl.anim.image.search.model.image.SimilarImageCollection;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

import static org.elasticsearch.index.query.QueryBuilders.*;
import static org.elasticsearch.index.query.functionscore.ScoreFunctionBuilders.scriptFunction;
import static pl.anim.image.search.analyser.EffigyIndexBuilder.createClient;

/**
 * ImageService is a class responsible for application logic and interactions with model.
 */
@Component
@SuppressWarnings("unchecked")
public class ImageService{

	private final Client client;

	@Value("${quantity.default}")
	private int defaultQuantity;
	@Value("${feature.default}")
	private String featureName;
	@Value("${feature.second}")
	private String secondFeature;

	private String elasticIndexName;

	private ScoreRepository scoreRepository;

	/**
	 * Default class constructor.
	 */
	@Autowired
	public ImageService(ScoreRepository scoreRepository) {
		this.client = createClient();
		this.elasticIndexName = EffigyIndexBuilder.getElasticIndexName();
		this.scoreRepository = scoreRepository;
	}

	/**
	 * Optional constructor when elasticsearch client is defined
	 *
	 * @param client elasticsearch client
	 */
	public ImageService(Client client) {
		this.client = client;
		this.elasticIndexName = EffigyIndexBuilder.getElasticIndexName();
	}

	/**
	 * Method that returns random image from elasticsearch.
	 *
	 * @return {@link ParentImage} instance representing image
	 */
	public ParentImage getRandomParentImage(String username) throws ParentImageNotFoundException, FullCycleOfParentImagesIsOver {

        QueryBuilder query = QueryBuilders.termQuery("test", "true");
		SearchResponse response = executeQuery(query, 1000);

		List<SearchHit> searchHitList = Arrays.asList(response.getHits().getHits());

        List<SearchHit> finalList = searchHitList.stream()
                .filter(searchHit -> scoreRepository.countByUsernameAndParentId(username, searchHit.getId()) < defaultQuantity)
                .collect(Collectors.toList());

        int randomHit;
        if(finalList.size() == 0){
            throw new FullCycleOfParentImagesIsOver();
        }
        else randomHit = ThreadLocalRandom.current()
				.nextInt((finalList.size()));

		SearchHit hit = finalList.get(randomHit);
		if(hit == null)
			throw  new ParentImageNotFoundException();

		String url = (String) hit.getSource().get("url");
//		System.out.println("Parent image with name - " + hit.getSource().get("name") + " with id - " + hit.getId());

		return new ParentImage(String.valueOf(hit.getId()), url);
	}

	/**
	 * Method to get similar images by some particular <a href = "http://www.lire-project.net/">LIRE</a> feature.
	 *
	 * @param parentId identification of the image for which we are searching for similar images
	 * @param imageFeatures list of features to use
	 * @param quantity number of similar images to return
	 * @return list of similar images
	 */
	public SimilarImageCollection getSimilarImagesByFeature(String parentId, ImageFeature[] imageFeatures, Integer quantity) {
		int finalQuantity = quantity == null ? defaultQuantity : quantity;
		String feature, sFeature;
        List<Integer> featureValues;

		if(imageFeatures == null || imageFeatures.length == 0 || imageFeatures.length > 2){
			feature = featureName;
			sFeature = secondFeature;
		}
		else if(imageFeatures.length == 1){
			feature = imageFeatures[0].name();
            sFeature = null;
		}
		else{
			feature = imageFeatures[0].name();
			sFeature = imageFeatures[1].name();
		}

		SearchResponse response = executeQuery(matchQuery("_id", parentId), 15);
		SearchHit hit = response.getHits().getHits()[0];
		featureValues = (List<Integer>) hit.getSource().get(feature);

		Map<String,Object> params = populateParams(feature, featureValues);
		List<Integer> secondFeatureValues = null;
		if(sFeature != null)
			secondFeatureValues = (List<Integer>)hit.getSource().get(sFeature);

		return new SimilarImageCollection(queryForSimilarImages(params,parentId,finalQuantity, sFeature, secondFeatureValues));
	}

	/**
	 * Method get list of similar images by parent image identification and number of similar images.
	 *
	 * @param parentId identification of the image for which we are searching for similar images
	 * @param quantity number of similar images to return
	 * @return list of similar images
	 */
	public SimilarImageCollection getSimilarImages(String parentId, Integer quantity) {
		return getSimilarImagesByFeature(parentId, new ImageFeature[]{}, quantity);
	}

	/**
	 * Method responsible for getting similar images to the image provided by user.
	 *
	 * @param file image received from user
	 * @return list of similar images
	 */
	public SimilarImageCollection getImageFromUser(MultipartFile file) {
		BufferedImage image = null;
		try {
			ByteArrayInputStream bis = new ByteArrayInputStream(file.getBytes());
			image = ImageIO.read(bis);
		} catch (IOException e) {
			e.printStackTrace();
		}
		String featureName = this.featureName;
		List<Integer> featureValues = ImagesAnalyser.getFeature(featureName,image);

		Map<String,Object> params = populateParams(featureName, featureValues);

		return new SimilarImageCollection(queryForSimilarImages(params,"abba",defaultQuantity, secondFeature, ImagesAnalyser.getFeature(secondFeature,image)));
	}

	/**
	 * Method which represents logic that is needed to make a query to elasticsearch to get similar images.
	 *
	 * @param params elasticsearch script parameters
	 * @param parentId identification of the image for which we are searching for similar images
	 * @param finalQuantity number of similar images to return
	 * @param secondFeatureValues parameter needed for better precision, determines vector of second feature values
	 * @return list of similar images
	 *
	 * @see ImageService#executeQuery(QueryBuilder, int)
	 * @see ImageService#getScript(Map)
	 * @see ImageService#getQueryResults(SearchResponse, int, String)
	 */
	private List<SimilarImage> queryForSimilarImages(Map<String,Object> params, String parentId, int finalQuantity, String sFeature, List<Integer> secondFeatureValues){
		final Script scriptForTheFirstQuery = getScript(params);
		QueryBuilder searchForSimilarImagesQuery = functionScoreQuery(scriptFunction(scriptForTheFirstQuery)).boostMode(CombineFunction.REPLACE);

		if(secondFeatureValues == null){
			return getQueryResults(executeQuery(searchForSimilarImagesQuery, 25), finalQuantity, parentId);
		}
		else{
			SearchResponse response = executeQuery(searchForSimilarImagesQuery, 25);
			List<Long> bestIds = new ArrayList<>();
			if (response.getHits() != null) {
				SearchHits searchHits = response.getHits();
				int counter = 0;
				for (SearchHit searchHit : searchHits.getHits()) {
					if (!searchHit.getId().equals(parentId)) {
						bestIds.add(Long.valueOf(searchHit.getId()));
						counter++;
					}

					if (counter == finalQuantity * 2) {
						break;
					}
				}
			}

            params = populateParams(sFeature, secondFeatureValues);

			final Script scriptForTheSecondQuery = getScript(params);
			ScriptScoreFunctionBuilder scoreFunction = ScoreFunctionBuilders.scriptFunction(scriptForTheSecondQuery);
			ConstantScoreQueryBuilder query = QueryBuilders.constantScoreQuery(termsQuery("_id",bestIds));
			QueryBuilder searchForSimilarImagesQueryForSecondFeature = functionScoreQuery(query, scoreFunction).boostMode(CombineFunction.REPLACE);

			return getQueryResults(executeQuery(searchForSimilarImagesQueryForSecondFeature, 15), finalQuantity, parentId);
		}
	}

	/**
	 * Method which performs specific queries to elasticsearch.
	 *
	 * @param query search query
	 * @return search response from elasticsearch
	 */
	private SearchResponse executeQuery(QueryBuilder query, int size){
		return client.prepareSearch(elasticIndexName)
				.setQuery(query)
                .setSize(size)
				.execute()
				.actionGet();
	}

	/**
	 * Method which returns an elasticsearch script reference to be used in the creation of queries.
	 *
	 * @param params elasticsearch script parameters
	 * @return instance of {@link Script} class
	 */
	private Script getScript(Map<String,Object> params){
		return new Script(ScriptType.INLINE,"native","effigy-helper",params);
	}

	/**
	 * Method to create {@link Map} with correct elasticsearch script parameters.
	 *
	 * @param name <a href = "http://www.lire-project.net/">LIRE</a> library feature name to compare
	 * @param values LIRE library feature vector values
	 * @return parameters for elasticsearch script
	 */
	private Map<String, Object> populateParams(String name, List<Integer> values){
		Map<String, Object> params = new HashMap<>();
		params.put("feature_name", name);
		params.put("feature_value", values);
		return params;
	}

	/**
	 * Method to analyse and return query results.
	 *
	 * @param response query search response
	 * @param finalQuantity number of similar images to return
	 * @param parentId identification of the image for which we are searching for similar images
	 * @return list of similar images
	 *
	 * @see SimilarImage
	 */
	private List<SimilarImage> getQueryResults(SearchResponse response, int finalQuantity, String parentId){
		List<SimilarImage> resultList = new ArrayList<>();
		if (response.getHits() != null) {
			SearchHits searchHits = response.getHits();
			int counter = 0;
			for (SearchHit searchHit : searchHits.getHits()) {
				if (!searchHit.getId().equals(parentId)) {
					resultList.add(new SimilarImage(searchHit.getId(), (String) searchHit.getSource().get("url"), counter + 1));
                    counter++;
				}

				if (counter == finalQuantity) {
					break;
				}
			}
		}
		return resultList;
	}
}
