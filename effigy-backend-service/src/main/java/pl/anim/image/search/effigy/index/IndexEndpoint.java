package pl.anim.image.search.effigy.index;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * Index controller which only serves basic html template for AngularJS frontend part.
 */
@Controller
public class IndexEndpoint {

	/**
	 * Method which responds for default browser request for server.
	 *
	 * @return basic <a href="http://jknack.github.io/handlebars.java/">handlebars</a> template
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/")
	public ModelAndView index() {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("index");

		return modelAndView;
	}
}
