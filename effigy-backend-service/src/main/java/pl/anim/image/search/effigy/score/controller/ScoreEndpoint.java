package pl.anim.image.search.effigy.score.controller;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import pl.anim.image.search.effigy.score.service.ScoreService;
import pl.anim.image.search.model.score.AdvanceScoreCountCollection;
import pl.anim.image.search.model.score.ScoreCount;
import pl.anim.image.search.model.score.UserScore;

/**
 * ScoreEndpoint is a REST controller responsible for all server actions connected with scores.
 */
@RestController
@RequestMapping(value = "/scores")
public class ScoreEndpoint {

	private final ScoreService scoreService;

	@Value("${user.session.identifier}")
	private String userIdentifier;

	/**
	 * Class constructor.
     *
	 * @param scoreService {@link ScoreService} instance responsible for logic and database interactions
	 */
	@Autowired
	public ScoreEndpoint(ScoreService scoreService) {
		this.scoreService = scoreService;
	}

	/**
	 * Method which saves user evaluation of an image.
     *
	 * @param userScore score received from user
	 */
	@RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@ApiOperation(value = "Save the user score")
	public void addScore(@RequestBody UserScore userScore) {
		scoreService.addScore(userScore);
	}

	/**
	 * Method which calculates total number of evaluated images.
     *
	 * @param username user unique identifier
	 * @return total number of images that a particular user evaluated
	 */
	@RequestMapping(value = "/count", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(value = "Get score count for the given user")
	public ScoreCount getScoreCount(@RequestParam(value = "username") String username) {
		return scoreService.getScoreCount(username);
	}

	/**
	 * Method which gets and returns back to user his score in comparison to other user scores.
     *
	 * @param username user unique identifier
	 * @return {@link AdvanceScoreCountCollection} instance
	 */
	@RequestMapping(value = "/count/best", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(value = "Get score count for the given user")
	public AdvanceScoreCountCollection getBestScoreCount(@RequestParam(value = "username") String username) {
		return scoreService.getBestScores(username);
	}
}
