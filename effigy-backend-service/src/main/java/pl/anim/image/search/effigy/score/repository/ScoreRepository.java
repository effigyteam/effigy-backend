package pl.anim.image.search.effigy.score.repository;

import org.springframework.data.repository.CrudRepository;
import pl.anim.image.search.model.score.UserScore;

public interface ScoreRepository extends CrudRepository<UserScore, String > {
	Long countByUsername(String username);
	Long countByUsernameAndParentId(String username, String parentId);
	Long countByParentId(String parentId);
}
