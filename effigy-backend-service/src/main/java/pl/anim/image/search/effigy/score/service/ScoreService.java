package pl.anim.image.search.effigy.score.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.anim.image.search.effigy.score.repository.ScoreRepository;
import pl.anim.image.search.effigy.utils.AggregationQueriesHelper;
import pl.anim.image.search.model.score.AdvanceScoreCountCollection;
import pl.anim.image.search.model.score.ScoreCount;
import pl.anim.image.search.model.score.UserScore;


/**
 * ScoreService is a class responsible for application logic and interactions with scores model.
 */
@Component
public class ScoreService {

	private final ScoreRepository scoreRepository;
	private final AggregationQueriesHelper helper;
    /**
     * Class constructor
     *
     * @param scoreRepository reference to the model which represents user scores
     */
	@Autowired
	public ScoreService(ScoreRepository scoreRepository, AggregationQueriesHelper helper) {
		this.scoreRepository = scoreRepository;
		this.helper = helper;
	}

    /**
     * Method which simple saves user evaluation of some image.
     *
     * @param userScore score provided by user and additional data {@link UserScore}
     */
	public void addScore(UserScore userScore) {
		scoreRepository.save(userScore);
	}

    /**
     * Method which calculates total number of images evaluations by particular user.
     *
     * @param username unique user identifier
     * @return total number of iamges evaluated
     */
	public ScoreCount getScoreCount(String username) {
		return new ScoreCount(scoreRepository.countByUsername(username).intValue());
	}

    /**
     * Method which gets top 3 scores among users and current user relative position.
     *
     * @param userName unique user identifier
     * @return {@link AdvanceScoreCountCollection} special data structure which holds necessary information
     */
	public AdvanceScoreCountCollection getBestScores(String userName){
		return helper.getBestScores(userName);
	}
}
