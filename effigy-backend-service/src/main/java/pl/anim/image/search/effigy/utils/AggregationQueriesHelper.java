package pl.anim.image.search.effigy.utils;

import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import pl.anim.image.search.effigy.score.repository.ScoreRepository;
import pl.anim.image.search.model.score.AdvanceScoreCount;
import pl.anim.image.search.model.score.AdvanceScoreCountCollection;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Class which is responsible for the usage of MongoDB aggregation framework in our applicaiton
 */
public class AggregationQueriesHelper {
    private final MongoClient client;
    private final String dbName;
    private ScoreRepository scoreRepository;

    private static final int GAP = 0;

    public AggregationQueriesHelper(MongoClient client, String dbName, ScoreRepository scoreRepository) {
        this.client = client;
        this.dbName = dbName;
        this.scoreRepository = scoreRepository;
    }
    public AdvanceScoreCountCollection getBestScores(String userName){
        List<AdvanceScoreCount> besties = new ArrayList<>();
        Long userScore = scoreRepository.countByUsername(userName);
        int limitOfScores = 5;

        Document group = new Document("$group", new BasicDBObject("_id", "$username")
                .append("number", new BasicDBObject("$sum", 1)));
        Document sort = new Document("$sort", new BasicDBObject("number", -1));

        List<Document> aggrOptions = new ArrayList<>();
        aggrOptions.add(group);
        aggrOptions.add(sort);
        MongoDatabase db = client.getDatabase(dbName);
        AggregateIterable<Document> output = db.getCollection("user_scores").aggregate(aggrOptions);
        int i = 0;
        long userPosition = 1;
        for (Document result : output) {
            if(i<limitOfScores){
                i++;
                besties.add(new AdvanceScoreCount(result.getString("_id"),result.getInteger("number")));
            }
            if(userScore<result.getInteger("number")){
                userPosition++;
            }
            else if(i>=10) break;
        }
        return new AdvanceScoreCountCollection(besties,userPosition,userName,userScore);
    }
    public Set<Integer> getScoresGroupedByParentId(){
        Set<Integer> set = new HashSet<>();
        Document group = new Document("$group", new Document("_id", "$parentId")
                .append("number_of_evaluations", new Document("$sum", 1)));
        Document sort = new Document("$sort", new Document("number_of_evaluations", 1));
        List<Document> aggrOptions = new ArrayList<>();
        aggrOptions.add(group);
        aggrOptions.add(sort);
        MongoCursor<Document> cursor  = client.getDatabase(dbName).getCollection("user_scores")
                                            .aggregate(aggrOptions)
                                            .iterator();
        int lowest = 0;
        if(cursor.hasNext()){
            Document doc = cursor.next();
            lowest = doc.getInteger("number_of_evaluations");
        }
        int current = lowest + GAP;
        while (cursor.hasNext()){
            Document doc = cursor.next();
            if(doc.getInteger("number_of_evaluations") > current){
                set.add(Integer.parseInt(doc.getString("_id")));
            }
        }
        return set;
    }
}
