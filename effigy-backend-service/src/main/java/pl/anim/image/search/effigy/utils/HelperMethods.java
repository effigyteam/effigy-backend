package pl.anim.image.search.effigy.utils;

import java.security.SecureRandom;

/**
 * HelpersMethods is a class which contains utility methods used by all parts of the application.
 *
 * @author Borys Komarov
 * @author Zahraa al-Taee
 */
public class HelperMethods {

    public static String randomString( int len ){
        String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-,.;:=+-/*_";
        SecureRandom rnd = new SecureRandom();
        StringBuilder sb = new StringBuilder( len );
        for( int i = 0; i < len; i++ )
            sb.append( AB.charAt( rnd.nextInt(AB.length()) ) );
        return sb.toString();
    }

}
