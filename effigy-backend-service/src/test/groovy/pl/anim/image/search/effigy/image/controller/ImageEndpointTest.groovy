package pl.anim.image.search.effigy.image.controller

import groovy.json.JsonSlurper
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.http.MediaType
import org.springframework.mock.web.MockMultipartFile
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import pl.anim.image.search.effigy.image.service.ImageService

import pl.anim.image.search.model.image.ParentImage
import pl.anim.image.search.model.image.SimilarImage
import pl.anim.image.search.model.image.SimilarImageCollection
import spock.lang.Specification

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*

@WebMvcTest(ImageEndpoint)
class ImageEndpointTest extends Specification{

    def controller
    def service
    def mockMvc

    def setup() {
        service = Mock(ImageService)
        controller = new ImageEndpoint(service)
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build()
    }

    def "should return random parent image and status ok"() {
        given:
        ParentImage image = new ParentImage("234", "image_url")

        when:
        def response = mockMvc.perform(get("/images/parent/random").param("username", "bob")
                .contentType(MediaType.APPLICATION_JSON))
        def result = response.andReturn().response
        def json = new JsonSlurper().parseText(result.contentAsString)
        then:
        response.andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith("application/json"))
        1 * service.getRandomParentImage(_) >> image
        with(json){
            parentId == "234"
            imageUrl == "image_url"
        }

    }

    def "should return similar images and status ok"(){
        given:
        def parentImageId = "77"
        def quantity = "3"
        def n = 3
        def similarImages = createSimilarImagesMock(n)

        when:
        def response = mockMvc.perform(get("/images")
                .param("parentId", parentImageId)
                .param("quantity", quantity))

        def result = response.andReturn().response
        def json = new JsonSlurper().parseText(result.contentAsString)

        then:
        response.andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith("application/json"))
        1 * service.getSimilarImages(_, _) >> similarImages
        json.similarImages.size() == n
    }

    def "should return similar images for the image submitted by user and status ok"(){
        given:
        MockMultipartFile file = new MockMultipartFile("image", "filename.txt", "text/plain", "some xml".getBytes());
        def n = 4
        def similarImages = createSimilarImagesMock(n)
        when:
        def response = mockMvc.perform(fileUpload("/images/file")
                .file(file))
        def result = response.andReturn().response
        def json = new JsonSlurper().parseText(result.contentAsString)

        then:
        response.andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith("application/json"))
        1 * service.getImageFromUser(_) >> similarImages
        json.similarImages.size() == n
    }

    def createSimilarImagesMock(int n){
        def list = new ArrayList<SimilarImage>()
        for(int i = 0; i < n; i++){
            SimilarImage img = new SimilarImage("id" + i, "image_url", i+1)
            list.add(img)
        }
        return new SimilarImageCollection(list)
    }
}
