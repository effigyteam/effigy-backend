package pl.anim.image.search.effigy.image.controller

import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import pl.anim.image.search.effigy.index.IndexEndpoint
import spock.lang.Specification

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view

@WebMvcTest(IndexEndpoint)
class IndexEndpointTest extends Specification{
    def controller
    def mockMvc

    def setup() {
        controller = new IndexEndpoint()
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build()
    }

    def "should return index view"(){
        when:
        def response = mockMvc.perform(get("/"))
        then:
        response.andExpect(view().name("index"))
    }
}
