package pl.anim.image.search.effigy.image.service

import org.elasticsearch.action.ListenableActionFuture
import org.elasticsearch.action.search.SearchRequestBuilder
import org.elasticsearch.action.search.SearchResponse
import org.elasticsearch.client.Client
import org.elasticsearch.common.bytes.BytesArray
import org.elasticsearch.common.text.Text
import org.elasticsearch.index.query.QueryBuilder
import org.elasticsearch.search.SearchHitField
import org.elasticsearch.search.internal.InternalSearchHit
import org.elasticsearch.search.internal.InternalSearchHitField
import org.elasticsearch.search.internal.InternalSearchHits
import org.junit.runner.RunWith
import org.springframework.test.context.junit4.SpringRunner
import pl.anim.image.search.effigy.score.repository.ScoreRepository
import spock.lang.Specification

class ImageServiceTest extends Specification {

    def source
    def searchHitArray
    def clientMock
    def builderMock
    def futureMock
    def responseMock
    def imageService

    def setup() {
        source = new BytesArray("{\"url\":\"image_url\"}");
        searchHitArray = new InternalSearchHit[5]

        for(int i = 1; i < 6; i++){
            def hit = new InternalSearchHit(i, "id" + i, new Text("image"), null)
            hit.sourceRef(source);
            searchHitArray[i-1] = hit;
        }


        clientMock = Mock(Client)
        builderMock = Mock(SearchRequestBuilder)
        futureMock = Mock(ListenableActionFuture)
        responseMock = Mock(SearchResponse)

        clientMock.prepareSearch(_ as String) >> builderMock
        builderMock.setTypes(_ as String) >> builderMock
        builderMock.setSize(_ as Integer) >> builderMock
        builderMock.setQuery(_ as QueryBuilder) >> builderMock
        builderMock.execute() >> futureMock
        futureMock.actionGet() >> responseMock
        responseMock.getHits() >> new InternalSearchHits(searchHitArray, 5, 1)

        imageService = new ImageService(clientMock)
    }

    def "should return collection of similar images"() {
        when:
        def result = imageService.getSimilarImages("parentId", null)

        then:
        result.similarImageList.size() == 5
        result.similarImageList.with {
            imageId == ["0", "1", "2", "3", "4"]
            imageUrl == ["image_url", "image_url", "image_url", "image_url", "image_url"]
            ranking == [1, 2, 3, 4, 5]
        }
    }

    def "should return collection of similar images given quantity"() {
        given:

        def searchHitArray = new InternalSearchHit[2]

        for(int i = 1; i < 3; i++){
            def hit = new InternalSearchHit(i, "id" + i, new Text("image"), null)
            hit.sourceRef(source);
            searchHitArray[i-1] = hit;
        }

        responseMock.getHits() >> new InternalSearchHits(searchHitArray, 2, 1)

        when:
        def result = imageService.getSimilarImagesByFeature("parentId", null, 2)

        then:
        result.similarImageList.size() == 2
        result.similarImageList.with {
            imageId == ["0", "1"]
            imageUrl == ["image_url", "image_url"]
            ranking == [1, 2]
        }
    }
}
