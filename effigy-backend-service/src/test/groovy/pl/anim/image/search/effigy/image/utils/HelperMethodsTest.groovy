package pl.anim.image.search.effigy.image.utils

import pl.anim.image.search.effigy.utils.HelperMethods
import spock.lang.Specification

class HelperMethodsTest extends Specification{
    def "should return random string with fixed length"(){
        when:
        def str1 = HelperMethods.randomString(5)
        def str2 = HelperMethods.randomString(5)
        def str3 = HelperMethods.randomString(10)
        then:
        str1.length() == 5
        str3.length() == 10
        str1 != str2
    }
}
